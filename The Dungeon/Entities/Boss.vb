﻿Public Class Boss
    Inherits Monster
    'The Bosses appear every 5 turns, can not be fled from, and guard the entrance to the next stage
    Sub New(ByVal mIndex As Integer)
        MyBase.New(-1)
        Select Case mIndex
            Case Else
                name = "3Rr0rBaU5"
                health = 60
                maxHealth = 60
                attack = 1
                defence = 1
                speed = 1
                inventory = {1}
        End Select
        pos = Game.player.pos
    End Sub
End Class
