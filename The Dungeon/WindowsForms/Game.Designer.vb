﻿<Global.Microsoft.VisualBasic.CompilerServices.DesignerGenerated()>
Partial Class Game
    Inherits System.Windows.Forms.Form

    'Form overrides dispose to clean up the component list.
    <System.Diagnostics.DebuggerNonUserCode()>
    Protected Overrides Sub Dispose(ByVal disposing As Boolean)
        Try
            If disposing AndAlso components IsNot Nothing Then
                components.Dispose()
            End If
        Finally
            MyBase.Dispose(disposing)
        End Try
    End Sub

    'Required by the Windows Form Designer
    Private components As System.ComponentModel.IContainer

    'NOTE: The following procedure is required by the Windows Form Designer
    'It can be modified using the Windows Form Designer.  
    'Do not modify it using the code editor.
    <System.Diagnostics.DebuggerStepThrough()>
    Private Sub InitializeComponent()
        Me.components = New System.ComponentModel.Container()
        Dim resources As System.ComponentModel.ComponentResourceManager = New System.ComponentModel.ComponentResourceManager(GetType(Game))
        Me.btnUse1 = New System.Windows.Forms.Button()
        Me.btnDrop = New System.Windows.Forms.Button()
        Me.btnLook = New System.Windows.Forms.Button()
        Me.picMarkBClothes = New System.Windows.Forms.PictureBox()
        Me.picMarkBRearHair2 = New System.Windows.Forms.PictureBox()
        Me.picMarkBRearHair1 = New System.Windows.Forms.PictureBox()
        Me.picMarkBFHair = New System.Windows.Forms.PictureBox()
        Me.btnControls = New System.Windows.Forms.Button()
        Me.btnChallengeBoss = New System.Windows.Forms.Button()
        Me.picEnemy = New System.Windows.Forms.PictureBox()
        Me.picMarkEyebrows = New System.Windows.Forms.PictureBox()
        Me.picFMarkBody = New System.Windows.Forms.PictureBox()
        Me.picfMarkClothes = New System.Windows.Forms.PictureBox()
        Me.picFMarkRHair2 = New System.Windows.Forms.PictureBox()
        Me.picFMarkRHair1 = New System.Windows.Forms.PictureBox()
        Me.picFMarkFHair = New System.Windows.Forms.PictureBox()
        Me.picMarkClothes = New System.Windows.Forms.PictureBox()
        Me.picMarkRHair2 = New System.Windows.Forms.PictureBox()
        Me.picMarkRHair1 = New System.Windows.Forms.PictureBox()
        Me.picMarkFHair = New System.Windows.Forms.PictureBox()
        Me.btnLeave = New System.Windows.Forms.Button()
        Me.btnFight = New System.Windows.Forms.Button()
        Me.btnNPCMG = New System.Windows.Forms.Button()
        Me.cboxNPCMG = New System.Windows.Forms.ComboBox()
        Me.btnShop = New System.Windows.Forms.Button()
        Me.btnTalk = New System.Windows.Forms.Button()
        Me.picNPC = New System.Windows.Forms.PictureBox()
        Me.picStatue = New System.Windows.Forms.PictureBox()
        Me.picChicken = New System.Windows.Forms.PictureBox()
        Me.picPlayerB = New System.Windows.Forms.PictureBox()
        Me.picChest = New System.Windows.Forms.PictureBox()
        Me.picStairs = New System.Windows.Forms.PictureBox()
        Me.picPlayer = New System.Windows.Forms.PictureBox()
        Me.picFog = New System.Windows.Forms.PictureBox()
        Me.picTile = New System.Windows.Forms.PictureBox()
        Me.btnL = New System.Windows.Forms.Button()
        Me.btnS = New System.Windows.Forms.Button()
        Me.picStart = New System.Windows.Forms.PictureBox()
        Me.btnEQP = New System.Windows.Forms.Button()
        Me.btnRUN = New System.Windows.Forms.Button()
        Me.btnMG = New System.Windows.Forms.Button()
        Me.cboxMG = New System.Windows.Forms.ComboBox()
        Me.btnATK = New System.Windows.Forms.Button()
        Me.lblEVD = New System.Windows.Forms.Label()
        Me.lblSPD = New System.Windows.Forms.Label()
        Me.lblSKL = New System.Windows.Forms.Label()
        Me.lblDEF = New System.Windows.Forms.Label()
        Me.lblATK = New System.Windows.Forms.Label()
        Me.lblLevel = New System.Windows.Forms.Label()
        Me.lblXP = New System.Windows.Forms.Label()
        Me.lblHunger = New System.Windows.Forms.Label()
        Me.lblMana = New System.Windows.Forms.Label()
        Me.lblHealth = New System.Windows.Forms.Label()
        Me.lblNameTitle = New System.Windows.Forms.Label()
        Me.Label1 = New System.Windows.Forms.Label()
        Me.btnUse = New System.Windows.Forms.Button()
        Me.lstInventory = New System.Windows.Forms.ListBox()
        Me.lstLog = New System.Windows.Forms.ListBox()
        Me.MenuStrip1 = New System.Windows.Forms.MenuStrip()
        Me.FileToolStripMenuItem = New System.Windows.Forms.ToolStripMenuItem()
        Me.SaveToolStripMenuItem = New System.Windows.Forms.ToolStripMenuItem()
        Me.LoadToolStripMenuItem = New System.Windows.Forms.ToolStripMenuItem()
        Me.NewGameToolStripMenuItem = New System.Windows.Forms.ToolStripMenuItem()
        Me.HelpToolStripMenuItem = New System.Windows.Forms.ToolStripMenuItem()
        Me.HelpToolStripMenuItem1 = New System.Windows.Forms.ToolStripMenuItem()
        Me.WASDArrowsMoveToolStripMenuItem = New System.Windows.Forms.ToolStripMenuItem()
        Me.InteractstairschestsToolStripMenuItem = New System.Windows.Forms.ToolStripMenuItem()
        Me.StatInfoToolStripMenuItem = New System.Windows.Forms.ToolStripMenuItem()
        Me.ReportToolStripMenuItem = New System.Windows.Forms.ToolStripMenuItem()
        Me.DebugToolStripMenuItem = New System.Windows.Forms.ToolStripMenuItem()
        Me.InfoToolStripMenuItem = New System.Windows.Forms.ToolStripMenuItem()
        Me.SettingsToolStripMenuItem = New System.Windows.Forms.ToolStripMenuItem()
        Me.picPortrait = New System.Windows.Forms.PictureBox()
        Me.btnEXM = New System.Windows.Forms.Button()
        Me.lblGold = New System.Windows.Forms.Label()
        Me.picShopkeepTile = New System.Windows.Forms.PictureBox()
        Me.picShopkeep = New System.Windows.Forms.PictureBox()
        Me.picSKPrin = New System.Windows.Forms.PictureBox()
        Me.picSKBunny = New System.Windows.Forms.PictureBox()
        Me.picLust1 = New System.Windows.Forms.PictureBox()
        Me.picLust2 = New System.Windows.Forms.PictureBox()
        Me.picLust3 = New System.Windows.Forms.PictureBox()
        Me.picLust4 = New System.Windows.Forms.PictureBox()
        Me.picLust5 = New System.Windows.Forms.PictureBox()
        Me.btnIns = New System.Windows.Forms.Button()
        Me.btnR = New System.Windows.Forms.Button()
        Me.btnU = New System.Windows.Forms.Button()
        Me.BtnD = New System.Windows.Forms.Button()
        Me.btnLft = New System.Windows.Forms.Button()
        Me.cmboxSpec = New System.Windows.Forms.ComboBox()
        Me.btnSpec = New System.Windows.Forms.Button()
        Me.btnFilter = New System.Windows.Forms.Button()
        Me.btnOk = New System.Windows.Forms.Button()
        Me.fUseable = New System.Windows.Forms.CheckBox()
        Me.fPotion = New System.Windows.Forms.CheckBox()
        Me.fFood = New System.Windows.Forms.CheckBox()
        Me.fArmor = New System.Windows.Forms.CheckBox()
        Me.fWeapon = New System.Windows.Forms.CheckBox()
        Me.fMisc = New System.Windows.Forms.CheckBox()
        Me.btnAll = New System.Windows.Forms.Button()
        Me.btnNone = New System.Windows.Forms.Button()
        Me.picTrap = New System.Windows.Forms.PictureBox()
        Me.btnSettings = New System.Windows.Forms.Button()
        Me.btnT = New System.Windows.Forms.Button()
        Me.Button1 = New System.Windows.Forms.Button()
        Me.btnS8 = New System.Windows.Forms.Button()
        Me.btnCancel = New System.Windows.Forms.Button()
        Me.btnS7 = New System.Windows.Forms.Button()
        Me.btnS6 = New System.Windows.Forms.Button()
        Me.btnS5 = New System.Windows.Forms.Button()
        Me.btnS4 = New System.Windows.Forms.Button()
        Me.btnS3 = New System.Windows.Forms.Button()
        Me.btnS2 = New System.Windows.Forms.Button()
        Me.btnS1 = New System.Windows.Forms.Button()
        Me.pnlSaveLoad = New System.Windows.Forms.Panel()
        Me.picSheep = New System.Windows.Forms.PictureBox()
        Me.picFrog = New System.Windows.Forms.PictureBox()
        Me.picTileF = New System.Windows.Forms.PictureBox()
        Me.picTree = New System.Windows.Forms.PictureBox()
        Me.picPlayerf = New System.Windows.Forms.PictureBox()
        Me.picLadderf = New System.Windows.Forms.PictureBox()
        Me.picChestf = New System.Windows.Forms.PictureBox()
        Me.picBimbof = New System.Windows.Forms.PictureBox()
        Me.picShopkeeperf = New System.Windows.Forms.PictureBox()
        Me.picStatuef = New System.Windows.Forms.PictureBox()
        Me.picTrapf = New System.Windows.Forms.PictureBox()
        Me.tmrKeyCD = New System.Windows.Forms.Timer(Me.components)
        Me.pnlCombat = New System.Windows.Forms.Panel()
        Me.lblCombatEvents = New System.Windows.Forms.TextBox()
        Me.Label2 = New System.Windows.Forms.Label()
        Me.lblPHealtDiff = New System.Windows.Forms.Label()
        Me.lblEHealthChange = New System.Windows.Forms.Label()
        Me.lblTurn = New System.Windows.Forms.Label()
        Me.lblPHealth = New System.Windows.Forms.Label()
        Me.lblPName = New System.Windows.Forms.Label()
        Me.lblEHealth = New System.Windows.Forms.Label()
        Me.lblEName = New System.Windows.Forms.Label()
        Me.picPHealth = New System.Windows.Forms.PictureBox()
        Me.picEHbar = New System.Windows.Forms.PictureBox()
        Me.lblEvent = New System.Windows.Forms.Label()
        Me.picLoadBar = New System.Windows.Forms.PictureBox()
        Me.btnWait = New System.Windows.Forms.Button()
        Me.picSWiz = New System.Windows.Forms.PictureBox()
        Me.picSWizF = New System.Windows.Forms.PictureBox()
        CType(Me.picMarkBClothes, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.picMarkBRearHair2, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.picMarkBRearHair1, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.picMarkBFHair, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.picEnemy, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.picMarkEyebrows, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.picFMarkBody, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.picfMarkClothes, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.picFMarkRHair2, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.picFMarkRHair1, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.picFMarkFHair, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.picMarkClothes, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.picMarkRHair2, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.picMarkRHair1, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.picMarkFHair, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.picNPC, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.picStatue, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.picChicken, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.picPlayerB, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.picChest, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.picStairs, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.picPlayer, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.picFog, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.picTile, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.picStart, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.MenuStrip1.SuspendLayout()
        CType(Me.picPortrait, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.picShopkeepTile, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.picShopkeep, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.picSKPrin, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.picSKBunny, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.picLust1, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.picLust2, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.picLust3, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.picLust4, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.picLust5, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.picTrap, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.pnlSaveLoad.SuspendLayout()
        CType(Me.picSheep, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.picFrog, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.picTileF, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.picTree, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.picPlayerf, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.picLadderf, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.picChestf, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.picBimbof, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.picShopkeeperf, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.picStatuef, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.picTrapf, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.pnlCombat.SuspendLayout()
        CType(Me.picPHealth, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.picEHbar, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.picLoadBar, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.picSWiz, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.picSWizF, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.SuspendLayout()
        '
        'btnUse1
        '
        Me.btnUse1.BackColor = System.Drawing.SystemColors.Window
        Me.btnUse1.Enabled = False
        Me.btnUse1.Font = New System.Drawing.Font("Consolas", 8.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.btnUse1.Location = New System.Drawing.Point(739, 607)
        Me.btnUse1.Name = "btnUse1"
        Me.btnUse1.Size = New System.Drawing.Size(75, 33)
        Me.btnUse1.TabIndex = 3
        Me.btnUse1.Text = "Use"
        Me.btnUse1.UseVisualStyleBackColor = False
        '
        'btnDrop
        '
        Me.btnDrop.BackColor = System.Drawing.SystemColors.Window
        Me.btnDrop.Enabled = False
        Me.btnDrop.Font = New System.Drawing.Font("Consolas", 7.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.btnDrop.Location = New System.Drawing.Point(820, 606)
        Me.btnDrop.Name = "btnDrop"
        Me.btnDrop.Size = New System.Drawing.Size(75, 34)
        Me.btnDrop.TabIndex = 4
        Me.btnDrop.Text = "Discard"
        Me.btnDrop.UseVisualStyleBackColor = False
        '
        'btnLook
        '
        Me.btnLook.BackColor = System.Drawing.SystemColors.Window
        Me.btnLook.Enabled = False
        Me.btnLook.Font = New System.Drawing.Font("Consolas", 7.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.btnLook.Location = New System.Drawing.Point(901, 606)
        Me.btnLook.Name = "btnLook"
        Me.btnLook.Size = New System.Drawing.Size(75, 34)
        Me.btnLook.TabIndex = 5
        Me.btnLook.Text = "Inspect"
        Me.btnLook.UseVisualStyleBackColor = False
        '
        'picMarkBClothes
        '
        Me.picMarkBClothes.BackgroundImage = CType(resources.GetObject("picMarkBClothes.BackgroundImage"), System.Drawing.Image)
        Me.picMarkBClothes.Location = New System.Drawing.Point(169, 94)
        Me.picMarkBClothes.Name = "picMarkBClothes"
        Me.picMarkBClothes.Size = New System.Drawing.Size(38, 39)
        Me.picMarkBClothes.TabIndex = 207
        Me.picMarkBClothes.TabStop = False
        Me.picMarkBClothes.Visible = False
        '
        'picMarkBRearHair2
        '
        Me.picMarkBRearHair2.BackgroundImage = CType(resources.GetObject("picMarkBRearHair2.BackgroundImage"), System.Drawing.Image)
        Me.picMarkBRearHair2.Location = New System.Drawing.Point(123, 94)
        Me.picMarkBRearHair2.Name = "picMarkBRearHair2"
        Me.picMarkBRearHair2.Size = New System.Drawing.Size(38, 39)
        Me.picMarkBRearHair2.TabIndex = 206
        Me.picMarkBRearHair2.TabStop = False
        Me.picMarkBRearHair2.Visible = False
        '
        'picMarkBRearHair1
        '
        Me.picMarkBRearHair1.BackgroundImage = CType(resources.GetObject("picMarkBRearHair1.BackgroundImage"), System.Drawing.Image)
        Me.picMarkBRearHair1.Location = New System.Drawing.Point(79, 94)
        Me.picMarkBRearHair1.Name = "picMarkBRearHair1"
        Me.picMarkBRearHair1.Size = New System.Drawing.Size(38, 39)
        Me.picMarkBRearHair1.TabIndex = 205
        Me.picMarkBRearHair1.TabStop = False
        Me.picMarkBRearHair1.Visible = False
        '
        'picMarkBFHair
        '
        Me.picMarkBFHair.BackgroundImage = CType(resources.GetObject("picMarkBFHair.BackgroundImage"), System.Drawing.Image)
        Me.picMarkBFHair.Location = New System.Drawing.Point(35, 93)
        Me.picMarkBFHair.Name = "picMarkBFHair"
        Me.picMarkBFHair.Size = New System.Drawing.Size(38, 39)
        Me.picMarkBFHair.TabIndex = 204
        Me.picMarkBFHair.TabStop = False
        Me.picMarkBFHair.Visible = False
        '
        'btnControls
        '
        Me.btnControls.BackColor = System.Drawing.Color.Black
        Me.btnControls.Cursor = System.Windows.Forms.Cursors.Hand
        Me.btnControls.Font = New System.Drawing.Font("Consolas", 8.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.btnControls.ForeColor = System.Drawing.Color.White
        Me.btnControls.Location = New System.Drawing.Point(335, 403)
        Me.btnControls.Name = "btnControls"
        Me.btnControls.Size = New System.Drawing.Size(99, 39)
        Me.btnControls.TabIndex = 203
        Me.btnControls.Text = "Controls"
        Me.btnControls.UseVisualStyleBackColor = False
        '
        'btnChallengeBoss
        '
        Me.btnChallengeBoss.AutoSize = True
        Me.btnChallengeBoss.BackColor = System.Drawing.Color.Black
        Me.btnChallengeBoss.Cursor = System.Windows.Forms.Cursors.Hand
        Me.btnChallengeBoss.Font = New System.Drawing.Font("Consolas", 8.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.btnChallengeBoss.ForeColor = System.Drawing.Color.White
        Me.btnChallengeBoss.Location = New System.Drawing.Point(254, 221)
        Me.btnChallengeBoss.Name = "btnChallengeBoss"
        Me.btnChallengeBoss.Size = New System.Drawing.Size(289, 39)
        Me.btnChallengeBoss.TabIndex = 202
        Me.btnChallengeBoss.Text = "Challenge Boss?"
        Me.btnChallengeBoss.UseVisualStyleBackColor = False
        Me.btnChallengeBoss.Visible = False
        '
        'picEnemy
        '
        Me.picEnemy.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Stretch
        Me.picEnemy.Location = New System.Drawing.Point(48, 203)
        Me.picEnemy.Name = "picEnemy"
        Me.picEnemy.Size = New System.Drawing.Size(125, 189)
        Me.picEnemy.TabIndex = 201
        Me.picEnemy.TabStop = False
        Me.picEnemy.Visible = False
        '
        'picMarkEyebrows
        '
        Me.picMarkEyebrows.BackgroundImage = CType(resources.GetObject("picMarkEyebrows.BackgroundImage"), System.Drawing.Image)
        Me.picMarkEyebrows.Location = New System.Drawing.Point(210, 139)
        Me.picMarkEyebrows.Name = "picMarkEyebrows"
        Me.picMarkEyebrows.Size = New System.Drawing.Size(38, 39)
        Me.picMarkEyebrows.TabIndex = 200
        Me.picMarkEyebrows.TabStop = False
        Me.picMarkEyebrows.Visible = False
        '
        'picFMarkBody
        '
        Me.picFMarkBody.BackgroundImage = CType(resources.GetObject("picFMarkBody.BackgroundImage"), System.Drawing.Image)
        Me.picFMarkBody.Location = New System.Drawing.Point(210, 184)
        Me.picFMarkBody.Name = "picFMarkBody"
        Me.picFMarkBody.Size = New System.Drawing.Size(38, 39)
        Me.picFMarkBody.TabIndex = 199
        Me.picFMarkBody.TabStop = False
        Me.picFMarkBody.Visible = False
        '
        'picfMarkClothes
        '
        Me.picfMarkClothes.BackgroundImage = CType(resources.GetObject("picfMarkClothes.BackgroundImage"), System.Drawing.Image)
        Me.picfMarkClothes.Location = New System.Drawing.Point(166, 184)
        Me.picfMarkClothes.Name = "picfMarkClothes"
        Me.picfMarkClothes.Size = New System.Drawing.Size(38, 39)
        Me.picfMarkClothes.TabIndex = 198
        Me.picfMarkClothes.TabStop = False
        Me.picfMarkClothes.Visible = False
        '
        'picFMarkRHair2
        '
        Me.picFMarkRHair2.BackgroundImage = CType(resources.GetObject("picFMarkRHair2.BackgroundImage"), System.Drawing.Image)
        Me.picFMarkRHair2.Location = New System.Drawing.Point(123, 184)
        Me.picFMarkRHair2.Name = "picFMarkRHair2"
        Me.picFMarkRHair2.Size = New System.Drawing.Size(38, 39)
        Me.picFMarkRHair2.TabIndex = 197
        Me.picFMarkRHair2.TabStop = False
        Me.picFMarkRHair2.Visible = False
        '
        'picFMarkRHair1
        '
        Me.picFMarkRHair1.BackgroundImage = CType(resources.GetObject("picFMarkRHair1.BackgroundImage"), System.Drawing.Image)
        Me.picFMarkRHair1.Location = New System.Drawing.Point(79, 184)
        Me.picFMarkRHair1.Name = "picFMarkRHair1"
        Me.picFMarkRHair1.Size = New System.Drawing.Size(38, 39)
        Me.picFMarkRHair1.TabIndex = 196
        Me.picFMarkRHair1.TabStop = False
        Me.picFMarkRHair1.Visible = False
        '
        'picFMarkFHair
        '
        Me.picFMarkFHair.BackgroundImage = CType(resources.GetObject("picFMarkFHair.BackgroundImage"), System.Drawing.Image)
        Me.picFMarkFHair.Location = New System.Drawing.Point(35, 184)
        Me.picFMarkFHair.Name = "picFMarkFHair"
        Me.picFMarkFHair.Size = New System.Drawing.Size(38, 39)
        Me.picFMarkFHair.TabIndex = 195
        Me.picFMarkFHair.TabStop = False
        Me.picFMarkFHair.Visible = False
        '
        'picMarkClothes
        '
        Me.picMarkClothes.BackgroundImage = CType(resources.GetObject("picMarkClothes.BackgroundImage"), System.Drawing.Image)
        Me.picMarkClothes.Location = New System.Drawing.Point(166, 139)
        Me.picMarkClothes.Name = "picMarkClothes"
        Me.picMarkClothes.Size = New System.Drawing.Size(38, 39)
        Me.picMarkClothes.TabIndex = 194
        Me.picMarkClothes.TabStop = False
        Me.picMarkClothes.Visible = False
        '
        'picMarkRHair2
        '
        Me.picMarkRHair2.BackgroundImage = CType(resources.GetObject("picMarkRHair2.BackgroundImage"), System.Drawing.Image)
        Me.picMarkRHair2.Location = New System.Drawing.Point(122, 139)
        Me.picMarkRHair2.Name = "picMarkRHair2"
        Me.picMarkRHair2.Size = New System.Drawing.Size(38, 39)
        Me.picMarkRHair2.TabIndex = 193
        Me.picMarkRHair2.TabStop = False
        Me.picMarkRHair2.Visible = False
        '
        'picMarkRHair1
        '
        Me.picMarkRHair1.BackgroundImage = CType(resources.GetObject("picMarkRHair1.BackgroundImage"), System.Drawing.Image)
        Me.picMarkRHair1.Location = New System.Drawing.Point(79, 139)
        Me.picMarkRHair1.Name = "picMarkRHair1"
        Me.picMarkRHair1.Size = New System.Drawing.Size(38, 39)
        Me.picMarkRHair1.TabIndex = 192
        Me.picMarkRHair1.TabStop = False
        Me.picMarkRHair1.Visible = False
        '
        'picMarkFHair
        '
        Me.picMarkFHair.BackgroundImage = CType(resources.GetObject("picMarkFHair.BackgroundImage"), System.Drawing.Image)
        Me.picMarkFHair.Location = New System.Drawing.Point(35, 139)
        Me.picMarkFHair.Name = "picMarkFHair"
        Me.picMarkFHair.Size = New System.Drawing.Size(38, 39)
        Me.picMarkFHair.TabIndex = 191
        Me.picMarkFHair.TabStop = False
        Me.picMarkFHair.Visible = False
        '
        'btnLeave
        '
        Me.btnLeave.BackColor = System.Drawing.Color.Black
        Me.btnLeave.Font = New System.Drawing.Font("Consolas", 8.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.btnLeave.ForeColor = System.Drawing.Color.White
        Me.btnLeave.Location = New System.Drawing.Point(584, 419)
        Me.btnLeave.Name = "btnLeave"
        Me.btnLeave.Size = New System.Drawing.Size(89, 36)
        Me.btnLeave.TabIndex = 186
        Me.btnLeave.Text = "Leave"
        Me.btnLeave.UseVisualStyleBackColor = False
        Me.btnLeave.Visible = False
        '
        'btnFight
        '
        Me.btnFight.BackColor = System.Drawing.Color.Black
        Me.btnFight.Font = New System.Drawing.Font("Consolas", 8.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.btnFight.ForeColor = System.Drawing.Color.White
        Me.btnFight.Location = New System.Drawing.Point(488, 419)
        Me.btnFight.Name = "btnFight"
        Me.btnFight.Size = New System.Drawing.Size(89, 36)
        Me.btnFight.TabIndex = 184
        Me.btnFight.Text = "Fight"
        Me.btnFight.UseVisualStyleBackColor = False
        Me.btnFight.Visible = False
        '
        'btnNPCMG
        '
        Me.btnNPCMG.BackColor = System.Drawing.Color.Black
        Me.btnNPCMG.Font = New System.Drawing.Font("Consolas", 8.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.btnNPCMG.ForeColor = System.Drawing.Color.White
        Me.btnNPCMG.Location = New System.Drawing.Point(392, 419)
        Me.btnNPCMG.Name = "btnNPCMG"
        Me.btnNPCMG.Size = New System.Drawing.Size(90, 36)
        Me.btnNPCMG.TabIndex = 183
        Me.btnNPCMG.Text = "Magic"
        Me.btnNPCMG.UseVisualStyleBackColor = False
        Me.btnNPCMG.Visible = False
        '
        'cboxNPCMG
        '
        Me.cboxNPCMG.BackColor = System.Drawing.Color.Black
        Me.cboxNPCMG.Font = New System.Drawing.Font("Consolas", 8.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.cboxNPCMG.ForeColor = System.Drawing.Color.White
        Me.cboxNPCMG.FormattingEnabled = True
        Me.cboxNPCMG.Location = New System.Drawing.Point(238, 419)
        Me.cboxNPCMG.Name = "cboxNPCMG"
        Me.cboxNPCMG.Size = New System.Drawing.Size(149, 27)
        Me.cboxNPCMG.TabIndex = 182
        Me.cboxNPCMG.Text = "-- Select --"
        Me.cboxNPCMG.Visible = False
        '
        'btnShop
        '
        Me.btnShop.BackColor = System.Drawing.Color.Black
        Me.btnShop.Font = New System.Drawing.Font("Consolas", 8.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.btnShop.ForeColor = System.Drawing.Color.White
        Me.btnShop.Location = New System.Drawing.Point(143, 419)
        Me.btnShop.Name = "btnShop"
        Me.btnShop.Size = New System.Drawing.Size(89, 36)
        Me.btnShop.TabIndex = 181
        Me.btnShop.Text = "Shop"
        Me.btnShop.UseVisualStyleBackColor = False
        Me.btnShop.Visible = False
        '
        'btnTalk
        '
        Me.btnTalk.BackColor = System.Drawing.Color.Black
        Me.btnTalk.Font = New System.Drawing.Font("Consolas", 8.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.btnTalk.ForeColor = System.Drawing.Color.White
        Me.btnTalk.Location = New System.Drawing.Point(48, 419)
        Me.btnTalk.Name = "btnTalk"
        Me.btnTalk.Size = New System.Drawing.Size(89, 36)
        Me.btnTalk.TabIndex = 180
        Me.btnTalk.Text = "Talk"
        Me.btnTalk.UseVisualStyleBackColor = False
        Me.btnTalk.Visible = False
        '
        'picNPC
        '
        Me.picNPC.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Stretch
        Me.picNPC.Location = New System.Drawing.Point(82, 179)
        Me.picNPC.Name = "picNPC"
        Me.picNPC.Size = New System.Drawing.Size(125, 189)
        Me.picNPC.TabIndex = 179
        Me.picNPC.TabStop = False
        Me.picNPC.Visible = False
        '
        'picStatue
        '
        Me.picStatue.BackgroundImage = CType(resources.GetObject("picStatue.BackgroundImage"), System.Drawing.Image)
        Me.picStatue.Location = New System.Drawing.Point(539, 50)
        Me.picStatue.Name = "picStatue"
        Me.picStatue.Size = New System.Drawing.Size(15, 15)
        Me.picStatue.TabIndex = 172
        Me.picStatue.TabStop = False
        Me.picStatue.Visible = False
        '
        'picChicken
        '
        Me.picChicken.BackgroundImage = CType(resources.GetObject("picChicken.BackgroundImage"), System.Drawing.Image)
        Me.picChicken.Location = New System.Drawing.Point(518, 50)
        Me.picChicken.Name = "picChicken"
        Me.picChicken.Size = New System.Drawing.Size(15, 15)
        Me.picChicken.TabIndex = 171
        Me.picChicken.TabStop = False
        Me.picChicken.Visible = False
        '
        'picPlayerB
        '
        Me.picPlayerB.BackgroundImage = CType(resources.GetObject("picPlayerB.BackgroundImage"), System.Drawing.Image)
        Me.picPlayerB.Location = New System.Drawing.Point(497, 50)
        Me.picPlayerB.Name = "picPlayerB"
        Me.picPlayerB.Size = New System.Drawing.Size(15, 15)
        Me.picPlayerB.TabIndex = 170
        Me.picPlayerB.TabStop = False
        Me.picPlayerB.Visible = False
        '
        'picChest
        '
        Me.picChest.BackgroundImage = CType(resources.GetObject("picChest.BackgroundImage"), System.Drawing.Image)
        Me.picChest.Location = New System.Drawing.Point(581, 29)
        Me.picChest.Name = "picChest"
        Me.picChest.Size = New System.Drawing.Size(15, 15)
        Me.picChest.TabIndex = 169
        Me.picChest.TabStop = False
        Me.picChest.Visible = False
        '
        'picStairs
        '
        Me.picStairs.BackgroundImage = CType(resources.GetObject("picStairs.BackgroundImage"), System.Drawing.Image)
        Me.picStairs.Location = New System.Drawing.Point(560, 29)
        Me.picStairs.Name = "picStairs"
        Me.picStairs.Size = New System.Drawing.Size(15, 15)
        Me.picStairs.TabIndex = 168
        Me.picStairs.TabStop = False
        Me.picStairs.Visible = False
        '
        'picPlayer
        '
        Me.picPlayer.BackgroundImage = CType(resources.GetObject("picPlayer.BackgroundImage"), System.Drawing.Image)
        Me.picPlayer.Location = New System.Drawing.Point(539, 29)
        Me.picPlayer.Name = "picPlayer"
        Me.picPlayer.Size = New System.Drawing.Size(15, 15)
        Me.picPlayer.TabIndex = 167
        Me.picPlayer.TabStop = False
        Me.picPlayer.Visible = False
        '
        'picFog
        '
        Me.picFog.BackColor = System.Drawing.Color.FromArgb(CType(CType(64, Byte), Integer), CType(CType(64, Byte), Integer), CType(CType(64, Byte), Integer))
        Me.picFog.BackgroundImage = CType(resources.GetObject("picFog.BackgroundImage"), System.Drawing.Image)
        Me.picFog.Location = New System.Drawing.Point(518, 29)
        Me.picFog.Name = "picFog"
        Me.picFog.Size = New System.Drawing.Size(15, 15)
        Me.picFog.TabIndex = 166
        Me.picFog.TabStop = False
        Me.picFog.Visible = False
        '
        'picTile
        '
        Me.picTile.BackgroundImage = CType(resources.GetObject("picTile.BackgroundImage"), System.Drawing.Image)
        Me.picTile.Location = New System.Drawing.Point(497, 29)
        Me.picTile.Name = "picTile"
        Me.picTile.Size = New System.Drawing.Size(15, 15)
        Me.picTile.TabIndex = 165
        Me.picTile.TabStop = False
        Me.picTile.Visible = False
        '
        'btnL
        '
        Me.btnL.BackColor = System.Drawing.Color.Black
        Me.btnL.Cursor = System.Windows.Forms.Cursors.Hand
        Me.btnL.Font = New System.Drawing.Font("Consolas", 8.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.btnL.ForeColor = System.Drawing.Color.White
        Me.btnL.Location = New System.Drawing.Point(335, 357)
        Me.btnL.Name = "btnL"
        Me.btnL.Size = New System.Drawing.Size(313, 39)
        Me.btnL.TabIndex = 163
        Me.btnL.Text = "Load Game"
        Me.btnL.UseVisualStyleBackColor = False
        '
        'btnS
        '
        Me.btnS.BackColor = System.Drawing.Color.Black
        Me.btnS.Cursor = System.Windows.Forms.Cursors.Hand
        Me.btnS.Font = New System.Drawing.Font("Consolas", 8.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.btnS.ForeColor = System.Drawing.Color.White
        Me.btnS.Location = New System.Drawing.Point(335, 311)
        Me.btnS.Name = "btnS"
        Me.btnS.Size = New System.Drawing.Size(313, 39)
        Me.btnS.TabIndex = 162
        Me.btnS.Text = "Start New Game"
        Me.btnS.UseVisualStyleBackColor = False
        '
        'picStart
        '
        Me.picStart.BackgroundImage = CType(resources.GetObject("picStart.BackgroundImage"), System.Drawing.Image)
        Me.picStart.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Stretch
        Me.picStart.Cursor = System.Windows.Forms.Cursors.Arrow
        Me.picStart.Location = New System.Drawing.Point(2, -12)
        Me.picStart.Name = "picStart"
        Me.picStart.Size = New System.Drawing.Size(1000, 701)
        Me.picStart.TabIndex = 160
        Me.picStart.TabStop = False
        '
        'btnEQP
        '
        Me.btnEQP.BackColor = System.Drawing.SystemColors.Window
        Me.btnEQP.Font = New System.Drawing.Font("Consolas", 8.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.btnEQP.Location = New System.Drawing.Point(734, 345)
        Me.btnEQP.Name = "btnEQP"
        Me.btnEQP.Size = New System.Drawing.Size(75, 33)
        Me.btnEQP.TabIndex = 161
        Me.btnEQP.Text = "Equip"
        Me.btnEQP.UseVisualStyleBackColor = False
        '
        'btnRUN
        '
        Me.btnRUN.BackColor = System.Drawing.Color.Black
        Me.btnRUN.Font = New System.Drawing.Font("Consolas", 8.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.btnRUN.ForeColor = System.Drawing.Color.White
        Me.btnRUN.Location = New System.Drawing.Point(597, 419)
        Me.btnRUN.Name = "btnRUN"
        Me.btnRUN.Size = New System.Drawing.Size(86, 36)
        Me.btnRUN.TabIndex = 159
        Me.btnRUN.Text = "Run"
        Me.btnRUN.UseVisualStyleBackColor = False
        Me.btnRUN.Visible = False
        '
        'btnMG
        '
        Me.btnMG.BackColor = System.Drawing.Color.Black
        Me.btnMG.Font = New System.Drawing.Font("Consolas", 8.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.btnMG.ForeColor = System.Drawing.Color.White
        Me.btnMG.Location = New System.Drawing.Point(392, 419)
        Me.btnMG.Name = "btnMG"
        Me.btnMG.Size = New System.Drawing.Size(90, 36)
        Me.btnMG.TabIndex = 158
        Me.btnMG.Text = "Magic"
        Me.btnMG.UseVisualStyleBackColor = False
        Me.btnMG.Visible = False
        '
        'cboxMG
        '
        Me.cboxMG.BackColor = System.Drawing.Color.Black
        Me.cboxMG.Font = New System.Drawing.Font("Consolas", 8.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.cboxMG.ForeColor = System.Drawing.Color.White
        Me.cboxMG.FormattingEnabled = True
        Me.cboxMG.Location = New System.Drawing.Point(237, 419)
        Me.cboxMG.Name = "cboxMG"
        Me.cboxMG.Size = New System.Drawing.Size(149, 27)
        Me.cboxMG.TabIndex = 157
        Me.cboxMG.Text = "-- Select --"
        Me.cboxMG.Visible = False
        '
        'btnATK
        '
        Me.btnATK.BackColor = System.Drawing.Color.Black
        Me.btnATK.Font = New System.Drawing.Font("Consolas", 8.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.btnATK.ForeColor = System.Drawing.Color.White
        Me.btnATK.Location = New System.Drawing.Point(118, 419)
        Me.btnATK.Name = "btnATK"
        Me.btnATK.Size = New System.Drawing.Size(89, 36)
        Me.btnATK.TabIndex = 156
        Me.btnATK.Text = "Attack"
        Me.btnATK.UseVisualStyleBackColor = False
        Me.btnATK.Visible = False
        '
        'lblEVD
        '
        Me.lblEVD.AutoSize = True
        Me.lblEVD.Font = New System.Drawing.Font("Consolas", 8.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.lblEVD.ForeColor = System.Drawing.Color.White
        Me.lblEVD.Location = New System.Drawing.Point(736, 252)
        Me.lblEVD.Name = "lblEVD"
        Me.lblEVD.Size = New System.Drawing.Size(99, 19)
        Me.lblEVD.TabIndex = 155
        Me.lblEVD.Text = "EVD = TEMP"
        '
        'lblSPD
        '
        Me.lblSPD.AutoSize = True
        Me.lblSPD.Font = New System.Drawing.Font("Consolas", 8.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.lblSPD.ForeColor = System.Drawing.Color.White
        Me.lblSPD.Location = New System.Drawing.Point(736, 233)
        Me.lblSPD.Name = "lblSPD"
        Me.lblSPD.Size = New System.Drawing.Size(99, 19)
        Me.lblSPD.TabIndex = 154
        Me.lblSPD.Text = "SPD = TEMP"
        '
        'lblSKL
        '
        Me.lblSKL.AutoSize = True
        Me.lblSKL.Font = New System.Drawing.Font("Consolas", 8.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.lblSKL.ForeColor = System.Drawing.Color.White
        Me.lblSKL.Location = New System.Drawing.Point(736, 214)
        Me.lblSKL.Name = "lblSKL"
        Me.lblSKL.Size = New System.Drawing.Size(99, 19)
        Me.lblSKL.TabIndex = 153
        Me.lblSKL.Text = "SKL = TEMP"
        '
        'lblDEF
        '
        Me.lblDEF.AutoSize = True
        Me.lblDEF.Font = New System.Drawing.Font("Consolas", 8.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.lblDEF.ForeColor = System.Drawing.Color.White
        Me.lblDEF.Location = New System.Drawing.Point(736, 195)
        Me.lblDEF.Name = "lblDEF"
        Me.lblDEF.Size = New System.Drawing.Size(99, 19)
        Me.lblDEF.TabIndex = 152
        Me.lblDEF.Text = "DEF = TEMP"
        '
        'lblATK
        '
        Me.lblATK.AutoSize = True
        Me.lblATK.Font = New System.Drawing.Font("Consolas", 8.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.lblATK.ForeColor = System.Drawing.Color.White
        Me.lblATK.Location = New System.Drawing.Point(736, 176)
        Me.lblATK.Name = "lblATK"
        Me.lblATK.Size = New System.Drawing.Size(99, 19)
        Me.lblATK.TabIndex = 151
        Me.lblATK.Text = "ATK = TEMP"
        '
        'lblLevel
        '
        Me.lblLevel.AutoSize = True
        Me.lblLevel.Font = New System.Drawing.Font("Consolas", 8.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.lblLevel.ForeColor = System.Drawing.Color.White
        Me.lblLevel.Location = New System.Drawing.Point(736, 157)
        Me.lblLevel.Name = "lblLevel"
        Me.lblLevel.Size = New System.Drawing.Size(99, 19)
        Me.lblLevel.TabIndex = 150
        Me.lblLevel.Text = "Level TEMP"
        Me.lblLevel.Visible = False
        '
        'lblXP
        '
        Me.lblXP.AutoSize = True
        Me.lblXP.Font = New System.Drawing.Font("Consolas", 8.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.lblXP.ForeColor = System.Drawing.Color.White
        Me.lblXP.Location = New System.Drawing.Point(736, 129)
        Me.lblXP.Name = "lblXP"
        Me.lblXP.Size = New System.Drawing.Size(162, 19)
        Me.lblXP.TabIndex = 149
        Me.lblXP.Text = "Expirience = TEMP"
        Me.lblXP.Visible = False
        '
        'lblHunger
        '
        Me.lblHunger.AutoSize = True
        Me.lblHunger.Font = New System.Drawing.Font("Consolas", 8.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.lblHunger.ForeColor = System.Drawing.Color.White
        Me.lblHunger.Location = New System.Drawing.Point(736, 110)
        Me.lblHunger.Name = "lblHunger"
        Me.lblHunger.Size = New System.Drawing.Size(126, 19)
        Me.lblHunger.TabIndex = 148
        Me.lblHunger.Text = "Hunger = TEMP"
        '
        'lblMana
        '
        Me.lblMana.AutoSize = True
        Me.lblMana.Font = New System.Drawing.Font("Consolas", 8.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.lblMana.ForeColor = System.Drawing.Color.White
        Me.lblMana.Location = New System.Drawing.Point(736, 91)
        Me.lblMana.Name = "lblMana"
        Me.lblMana.Size = New System.Drawing.Size(108, 19)
        Me.lblMana.TabIndex = 147
        Me.lblMana.Text = "Mana = TEMP"
        '
        'lblHealth
        '
        Me.lblHealth.AutoSize = True
        Me.lblHealth.Font = New System.Drawing.Font("Consolas", 8.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.lblHealth.ForeColor = System.Drawing.Color.White
        Me.lblHealth.Location = New System.Drawing.Point(736, 72)
        Me.lblHealth.Name = "lblHealth"
        Me.lblHealth.Size = New System.Drawing.Size(126, 19)
        Me.lblHealth.TabIndex = 146
        Me.lblHealth.Text = "Health = TEMP"
        '
        'lblNameTitle
        '
        Me.lblNameTitle.AutoSize = True
        Me.lblNameTitle.BackColor = System.Drawing.Color.Black
        Me.lblNameTitle.Font = New System.Drawing.Font("Consolas", 9.25!, System.Drawing.FontStyle.Underline, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.lblNameTitle.ForeColor = System.Drawing.Color.White
        Me.lblNameTitle.Location = New System.Drawing.Point(732, 40)
        Me.lblNameTitle.Name = "lblNameTitle"
        Me.lblNameTitle.Size = New System.Drawing.Size(160, 22)
        Me.lblNameTitle.TabIndex = 145
        Me.lblNameTitle.Text = "PLACEHOLDERTEXT"
        '
        'Label1
        '
        Me.Label1.AutoSize = True
        Me.Label1.BackColor = System.Drawing.Color.Black
        Me.Label1.Font = New System.Drawing.Font("Consolas", 9.75!, System.Drawing.FontStyle.Underline, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Label1.ForeColor = System.Drawing.Color.White
        Me.Label1.Location = New System.Drawing.Point(736, 391)
        Me.Label1.Name = "Label1"
        Me.Label1.Size = New System.Drawing.Size(120, 23)
        Me.Label1.TabIndex = 144
        Me.Label1.Text = "Inventory:" & Global.Microsoft.VisualBasic.ChrW(13) & Global.Microsoft.VisualBasic.ChrW(10)
        '
        'btnUse
        '
        Me.btnUse.BackColor = System.Drawing.SystemColors.Window
        Me.btnUse.Enabled = False
        Me.btnUse.Font = New System.Drawing.Font("Consolas", 8.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.btnUse.Location = New System.Drawing.Point(740, 606)
        Me.btnUse.Name = "btnUse"
        Me.btnUse.Size = New System.Drawing.Size(75, 33)
        Me.btnUse.TabIndex = 143
        Me.btnUse.Text = "Use"
        Me.btnUse.UseVisualStyleBackColor = False
        '
        'lstInventory
        '
        Me.lstInventory.BackColor = System.Drawing.Color.Black
        Me.lstInventory.Font = New System.Drawing.Font("Consolas", 8.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.lstInventory.ForeColor = System.Drawing.Color.White
        Me.lstInventory.FormattingEnabled = True
        Me.lstInventory.ItemHeight = 19
        Me.lstInventory.Location = New System.Drawing.Point(734, 419)
        Me.lstInventory.Name = "lstInventory"
        Me.lstInventory.Size = New System.Drawing.Size(262, 156)
        Me.lstInventory.TabIndex = 142
        '
        'lstLog
        '
        Me.lstLog.BackColor = System.Drawing.Color.Black
        Me.lstLog.Font = New System.Drawing.Font("Consolas", 7.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.lstLog.ForeColor = System.Drawing.Color.White
        Me.lstLog.FormattingEnabled = True
        Me.lstLog.ItemHeight = 17
        Me.lstLog.Location = New System.Drawing.Point(13, 510)
        Me.lstLog.Name = "lstLog"
        Me.lstLog.Size = New System.Drawing.Size(701, 174)
        Me.lstLog.TabIndex = 141
        '
        'MenuStrip1
        '
        Me.MenuStrip1.BackColor = System.Drawing.Color.Black
        Me.MenuStrip1.Dock = System.Windows.Forms.DockStyle.None
        Me.MenuStrip1.ImageScalingSize = New System.Drawing.Size(24, 24)
        Me.MenuStrip1.Items.AddRange(New System.Windows.Forms.ToolStripItem() {Me.FileToolStripMenuItem, Me.HelpToolStripMenuItem, Me.SettingsToolStripMenuItem})
        Me.MenuStrip1.Location = New System.Drawing.Point(3, 4)
        Me.MenuStrip1.Name = "MenuStrip1"
        Me.MenuStrip1.Size = New System.Drawing.Size(132, 30)
        Me.MenuStrip1.TabIndex = 140
        Me.MenuStrip1.Text = "MenuStrip1"
        '
        'FileToolStripMenuItem
        '
        Me.FileToolStripMenuItem.BackColor = System.Drawing.Color.Black
        Me.FileToolStripMenuItem.DropDownItems.AddRange(New System.Windows.Forms.ToolStripItem() {Me.SaveToolStripMenuItem, Me.LoadToolStripMenuItem, Me.NewGameToolStripMenuItem})
        Me.FileToolStripMenuItem.Font = New System.Drawing.Font("Consolas", 9.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.FileToolStripMenuItem.ForeColor = System.Drawing.Color.White
        Me.FileToolStripMenuItem.Name = "FileToolStripMenuItem"
        Me.FileToolStripMenuItem.Size = New System.Drawing.Size(62, 26)
        Me.FileToolStripMenuItem.Text = "File"
        '
        'SaveToolStripMenuItem
        '
        Me.SaveToolStripMenuItem.BackColor = System.Drawing.Color.Black
        Me.SaveToolStripMenuItem.ForeColor = System.Drawing.Color.White
        Me.SaveToolStripMenuItem.Name = "SaveToolStripMenuItem"
        Me.SaveToolStripMenuItem.Size = New System.Drawing.Size(172, 30)
        Me.SaveToolStripMenuItem.Text = "Save"
        '
        'LoadToolStripMenuItem
        '
        Me.LoadToolStripMenuItem.BackColor = System.Drawing.Color.Black
        Me.LoadToolStripMenuItem.ForeColor = System.Drawing.Color.White
        Me.LoadToolStripMenuItem.Name = "LoadToolStripMenuItem"
        Me.LoadToolStripMenuItem.Size = New System.Drawing.Size(172, 30)
        Me.LoadToolStripMenuItem.Text = "Load"
        '
        'NewGameToolStripMenuItem
        '
        Me.NewGameToolStripMenuItem.BackColor = System.Drawing.Color.Black
        Me.NewGameToolStripMenuItem.ForeColor = System.Drawing.Color.White
        Me.NewGameToolStripMenuItem.Name = "NewGameToolStripMenuItem"
        Me.NewGameToolStripMenuItem.Size = New System.Drawing.Size(172, 30)
        Me.NewGameToolStripMenuItem.Text = "New Game"
        '
        'HelpToolStripMenuItem
        '
        Me.HelpToolStripMenuItem.DropDownItems.AddRange(New System.Windows.Forms.ToolStripItem() {Me.HelpToolStripMenuItem1, Me.StatInfoToolStripMenuItem, Me.ReportToolStripMenuItem, Me.DebugToolStripMenuItem, Me.InfoToolStripMenuItem})
        Me.HelpToolStripMenuItem.Font = New System.Drawing.Font("Consolas", 9.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.HelpToolStripMenuItem.ForeColor = System.Drawing.Color.White
        Me.HelpToolStripMenuItem.Name = "HelpToolStripMenuItem"
        Me.HelpToolStripMenuItem.Size = New System.Drawing.Size(62, 26)
        Me.HelpToolStripMenuItem.Text = "Help"
        '
        'HelpToolStripMenuItem1
        '
        Me.HelpToolStripMenuItem1.BackColor = System.Drawing.Color.Black
        Me.HelpToolStripMenuItem1.DropDownItems.AddRange(New System.Windows.Forms.ToolStripItem() {Me.WASDArrowsMoveToolStripMenuItem, Me.InteractstairschestsToolStripMenuItem})
        Me.HelpToolStripMenuItem1.ForeColor = System.Drawing.Color.White
        Me.HelpToolStripMenuItem1.Name = "HelpToolStripMenuItem1"
        Me.HelpToolStripMenuItem1.Size = New System.Drawing.Size(182, 30)
        Me.HelpToolStripMenuItem1.Text = "Controls"
        '
        'WASDArrowsMoveToolStripMenuItem
        '
        Me.WASDArrowsMoveToolStripMenuItem.BackColor = System.Drawing.Color.Black
        Me.WASDArrowsMoveToolStripMenuItem.ForeColor = System.Drawing.Color.White
        Me.WASDArrowsMoveToolStripMenuItem.Name = "WASDArrowsMoveToolStripMenuItem"
        Me.WASDArrowsMoveToolStripMenuItem.Size = New System.Drawing.Size(402, 30)
        Me.WASDArrowsMoveToolStripMenuItem.Text = "w, a, s, d / arrows = move"
        '
        'InteractstairschestsToolStripMenuItem
        '
        Me.InteractstairschestsToolStripMenuItem.BackColor = System.Drawing.Color.Black
        Me.InteractstairschestsToolStripMenuItem.ForeColor = System.Drawing.Color.White
        Me.InteractstairschestsToolStripMenuItem.Name = "InteractstairschestsToolStripMenuItem"
        Me.InteractstairschestsToolStripMenuItem.Size = New System.Drawing.Size(402, 30)
        Me.InteractstairschestsToolStripMenuItem.Text = "e / ; = interact(stairs/chests)"
        '
        'StatInfoToolStripMenuItem
        '
        Me.StatInfoToolStripMenuItem.BackColor = System.Drawing.Color.Black
        Me.StatInfoToolStripMenuItem.ForeColor = System.Drawing.Color.White
        Me.StatInfoToolStripMenuItem.Name = "StatInfoToolStripMenuItem"
        Me.StatInfoToolStripMenuItem.Size = New System.Drawing.Size(182, 30)
        Me.StatInfoToolStripMenuItem.Text = "Stat Info"
        Me.StatInfoToolStripMenuItem.Visible = False
        '
        'ReportToolStripMenuItem
        '
        Me.ReportToolStripMenuItem.BackColor = System.Drawing.Color.Black
        Me.ReportToolStripMenuItem.ForeColor = System.Drawing.Color.White
        Me.ReportToolStripMenuItem.Name = "ReportToolStripMenuItem"
        Me.ReportToolStripMenuItem.Size = New System.Drawing.Size(182, 30)
        Me.ReportToolStripMenuItem.Text = "Report"
        '
        'DebugToolStripMenuItem
        '
        Me.DebugToolStripMenuItem.BackColor = System.Drawing.Color.Black
        Me.DebugToolStripMenuItem.ForeColor = System.Drawing.Color.White
        Me.DebugToolStripMenuItem.Name = "DebugToolStripMenuItem"
        Me.DebugToolStripMenuItem.Size = New System.Drawing.Size(182, 30)
        Me.DebugToolStripMenuItem.Text = "Debug"
        '
        'InfoToolStripMenuItem
        '
        Me.InfoToolStripMenuItem.BackColor = System.Drawing.Color.Black
        Me.InfoToolStripMenuItem.Font = New System.Drawing.Font("Consolas", 9.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.InfoToolStripMenuItem.ForeColor = System.Drawing.Color.White
        Me.InfoToolStripMenuItem.Name = "InfoToolStripMenuItem"
        Me.InfoToolStripMenuItem.Size = New System.Drawing.Size(182, 30)
        Me.InfoToolStripMenuItem.Text = "Info"
        '
        'SettingsToolStripMenuItem
        '
        Me.SettingsToolStripMenuItem.Font = New System.Drawing.Font("Consolas", 9.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.SettingsToolStripMenuItem.ForeColor = System.Drawing.Color.White
        Me.SettingsToolStripMenuItem.Name = "SettingsToolStripMenuItem"
        Me.SettingsToolStripMenuItem.Size = New System.Drawing.Size(102, 26)
        Me.SettingsToolStripMenuItem.Text = "Settings"
        Me.SettingsToolStripMenuItem.Visible = False
        '
        'picPortrait
        '
        Me.picPortrait.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Stretch
        Me.picPortrait.Location = New System.Drawing.Point(871, 143)
        Me.picPortrait.Name = "picPortrait"
        Me.picPortrait.Size = New System.Drawing.Size(125, 189)
        Me.picPortrait.TabIndex = 208
        Me.picPortrait.TabStop = False
        '
        'btnEXM
        '
        Me.btnEXM.BackColor = System.Drawing.SystemColors.Window
        Me.btnEXM.Font = New System.Drawing.Font("Consolas", 8.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.btnEXM.Location = New System.Drawing.Point(860, 345)
        Me.btnEXM.Name = "btnEXM"
        Me.btnEXM.Size = New System.Drawing.Size(136, 33)
        Me.btnEXM.TabIndex = 209
        Me.btnEXM.Text = "Examine Self"
        Me.btnEXM.UseVisualStyleBackColor = False
        '
        'lblGold
        '
        Me.lblGold.AutoSize = True
        Me.lblGold.Font = New System.Drawing.Font("Consolas", 8.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.lblGold.ForeColor = System.Drawing.Color.White
        Me.lblGold.Location = New System.Drawing.Point(736, 271)
        Me.lblGold.Name = "lblGold"
        Me.lblGold.Size = New System.Drawing.Size(135, 19)
        Me.lblGold.TabIndex = 210
        Me.lblGold.Text = "GOLD = 999999+"
        '
        'picShopkeepTile
        '
        Me.picShopkeepTile.BackgroundImage = CType(resources.GetObject("picShopkeepTile.BackgroundImage"), System.Drawing.Image)
        Me.picShopkeepTile.Location = New System.Drawing.Point(560, 50)
        Me.picShopkeepTile.Name = "picShopkeepTile"
        Me.picShopkeepTile.Size = New System.Drawing.Size(15, 15)
        Me.picShopkeepTile.TabIndex = 211
        Me.picShopkeepTile.TabStop = False
        Me.picShopkeepTile.Visible = False
        '
        'picShopkeep
        '
        Me.picShopkeep.BackgroundImage = CType(resources.GetObject("picShopkeep.BackgroundImage"), System.Drawing.Image)
        Me.picShopkeep.Location = New System.Drawing.Point(233, 50)
        Me.picShopkeep.Name = "picShopkeep"
        Me.picShopkeep.Size = New System.Drawing.Size(15, 15)
        Me.picShopkeep.TabIndex = 212
        Me.picShopkeep.TabStop = False
        Me.picShopkeep.Visible = False
        '
        'picSKPrin
        '
        Me.picSKPrin.BackgroundImage = CType(resources.GetObject("picSKPrin.BackgroundImage"), System.Drawing.Image)
        Me.picSKPrin.Location = New System.Drawing.Point(296, 50)
        Me.picSKPrin.Name = "picSKPrin"
        Me.picSKPrin.Size = New System.Drawing.Size(15, 15)
        Me.picSKPrin.TabIndex = 213
        Me.picSKPrin.TabStop = False
        Me.picSKPrin.Visible = False
        '
        'picSKBunny
        '
        Me.picSKBunny.BackgroundImage = CType(resources.GetObject("picSKBunny.BackgroundImage"), System.Drawing.Image)
        Me.picSKBunny.Location = New System.Drawing.Point(317, 50)
        Me.picSKBunny.Name = "picSKBunny"
        Me.picSKBunny.Size = New System.Drawing.Size(15, 15)
        Me.picSKBunny.TabIndex = 214
        Me.picSKBunny.TabStop = False
        Me.picSKBunny.Visible = False
        '
        'picLust1
        '
        Me.picLust1.BackgroundImage = CType(resources.GetObject("picLust1.BackgroundImage"), System.Drawing.Image)
        Me.picLust1.Location = New System.Drawing.Point(371, 29)
        Me.picLust1.Name = "picLust1"
        Me.picLust1.Size = New System.Drawing.Size(15, 15)
        Me.picLust1.TabIndex = 215
        Me.picLust1.TabStop = False
        Me.picLust1.Visible = False
        '
        'picLust2
        '
        Me.picLust2.BackgroundImage = CType(resources.GetObject("picLust2.BackgroundImage"), System.Drawing.Image)
        Me.picLust2.Location = New System.Drawing.Point(392, 29)
        Me.picLust2.Name = "picLust2"
        Me.picLust2.Size = New System.Drawing.Size(15, 15)
        Me.picLust2.TabIndex = 216
        Me.picLust2.TabStop = False
        Me.picLust2.Visible = False
        '
        'picLust3
        '
        Me.picLust3.BackgroundImage = CType(resources.GetObject("picLust3.BackgroundImage"), System.Drawing.Image)
        Me.picLust3.Location = New System.Drawing.Point(413, 29)
        Me.picLust3.Name = "picLust3"
        Me.picLust3.Size = New System.Drawing.Size(15, 15)
        Me.picLust3.TabIndex = 217
        Me.picLust3.TabStop = False
        Me.picLust3.Visible = False
        '
        'picLust4
        '
        Me.picLust4.BackgroundImage = CType(resources.GetObject("picLust4.BackgroundImage"), System.Drawing.Image)
        Me.picLust4.Location = New System.Drawing.Point(372, 50)
        Me.picLust4.Name = "picLust4"
        Me.picLust4.Size = New System.Drawing.Size(15, 15)
        Me.picLust4.TabIndex = 218
        Me.picLust4.TabStop = False
        Me.picLust4.Visible = False
        '
        'picLust5
        '
        Me.picLust5.Location = New System.Drawing.Point(393, 50)
        Me.picLust5.Name = "picLust5"
        Me.picLust5.Size = New System.Drawing.Size(15, 15)
        Me.picLust5.TabIndex = 219
        Me.picLust5.TabStop = False
        Me.picLust5.Visible = False
        '
        'btnIns
        '
        Me.btnIns.BackColor = System.Drawing.SystemColors.Window
        Me.btnIns.Font = New System.Drawing.Font("Consolas", 8.0!)
        Me.btnIns.Location = New System.Drawing.Point(939, 646)
        Me.btnIns.Name = "btnIns"
        Me.btnIns.Size = New System.Drawing.Size(38, 33)
        Me.btnIns.TabIndex = 230
        Me.btnIns.Text = ";"
        Me.btnIns.UseVisualStyleBackColor = False
        '
        'btnR
        '
        Me.btnR.BackColor = System.Drawing.SystemColors.Window
        Me.btnR.Font = New System.Drawing.Font("Consolas", 8.0!)
        Me.btnR.Location = New System.Drawing.Point(873, 646)
        Me.btnR.Name = "btnR"
        Me.btnR.Size = New System.Drawing.Size(38, 33)
        Me.btnR.TabIndex = 229
        Me.btnR.Text = ">"
        Me.btnR.UseVisualStyleBackColor = False
        '
        'btnU
        '
        Me.btnU.BackColor = System.Drawing.SystemColors.Window
        Me.btnU.Font = New System.Drawing.Font("Consolas", 8.0!)
        Me.btnU.Location = New System.Drawing.Point(829, 646)
        Me.btnU.Name = "btnU"
        Me.btnU.Size = New System.Drawing.Size(38, 33)
        Me.btnU.TabIndex = 228
        Me.btnU.Text = "ʌ"
        Me.btnU.UseVisualStyleBackColor = False
        '
        'BtnD
        '
        Me.BtnD.BackColor = System.Drawing.SystemColors.Window
        Me.BtnD.Font = New System.Drawing.Font("Consolas", 8.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.BtnD.Location = New System.Drawing.Point(785, 646)
        Me.BtnD.Name = "BtnD"
        Me.BtnD.Size = New System.Drawing.Size(38, 33)
        Me.BtnD.TabIndex = 227
        Me.BtnD.Text = "v"
        Me.BtnD.UseVisualStyleBackColor = False
        '
        'btnLft
        '
        Me.btnLft.BackColor = System.Drawing.SystemColors.Window
        Me.btnLft.Font = New System.Drawing.Font("Consolas", 8.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.btnLft.Location = New System.Drawing.Point(741, 646)
        Me.btnLft.Name = "btnLft"
        Me.btnLft.Size = New System.Drawing.Size(38, 33)
        Me.btnLft.TabIndex = 226
        Me.btnLft.Text = "<"
        Me.btnLft.UseVisualStyleBackColor = False
        '
        'cmboxSpec
        '
        Me.cmboxSpec.BackColor = System.Drawing.Color.Black
        Me.cmboxSpec.Font = New System.Drawing.Font("Consolas", 8.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.cmboxSpec.ForeColor = System.Drawing.Color.White
        Me.cmboxSpec.FormattingEnabled = True
        Me.cmboxSpec.Location = New System.Drawing.Point(237, 461)
        Me.cmboxSpec.Name = "cmboxSpec"
        Me.cmboxSpec.Size = New System.Drawing.Size(149, 27)
        Me.cmboxSpec.TabIndex = 231
        Me.cmboxSpec.Text = "-- Select --"
        Me.cmboxSpec.Visible = False
        '
        'btnSpec
        '
        Me.btnSpec.BackColor = System.Drawing.Color.Black
        Me.btnSpec.Font = New System.Drawing.Font("Consolas", 8.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.btnSpec.ForeColor = System.Drawing.Color.White
        Me.btnSpec.Location = New System.Drawing.Point(392, 461)
        Me.btnSpec.Name = "btnSpec"
        Me.btnSpec.Size = New System.Drawing.Size(90, 36)
        Me.btnSpec.TabIndex = 232
        Me.btnSpec.Text = "Special"
        Me.btnSpec.UseVisualStyleBackColor = False
        Me.btnSpec.Visible = False
        '
        'btnFilter
        '
        Me.btnFilter.BackColor = System.Drawing.SystemColors.Window
        Me.btnFilter.Font = New System.Drawing.Font("Consolas", 8.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.btnFilter.Location = New System.Drawing.Point(888, 384)
        Me.btnFilter.Name = "btnFilter"
        Me.btnFilter.Size = New System.Drawing.Size(108, 28)
        Me.btnFilter.TabIndex = 233
        Me.btnFilter.Text = "Filter"
        Me.btnFilter.UseVisualStyleBackColor = False
        '
        'btnOk
        '
        Me.btnOk.BackColor = System.Drawing.SystemColors.Window
        Me.btnOk.Font = New System.Drawing.Font("Consolas", 8.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.btnOk.Location = New System.Drawing.Point(820, 557)
        Me.btnOk.Name = "btnOk"
        Me.btnOk.Size = New System.Drawing.Size(108, 29)
        Me.btnOk.TabIndex = 235
        Me.btnOk.Text = "Ok"
        Me.btnOk.UseVisualStyleBackColor = False
        Me.btnOk.Visible = False
        '
        'fUseable
        '
        Me.fUseable.AutoSize = True
        Me.fUseable.Checked = True
        Me.fUseable.CheckState = System.Windows.Forms.CheckState.Checked
        Me.fUseable.Font = New System.Drawing.Font("Consolas", 8.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.fUseable.ForeColor = System.Drawing.Color.White
        Me.fUseable.Location = New System.Drawing.Point(738, 422)
        Me.fUseable.Name = "fUseable"
        Me.fUseable.Size = New System.Drawing.Size(98, 23)
        Me.fUseable.TabIndex = 236
        Me.fUseable.Text = "Useable"
        Me.fUseable.UseVisualStyleBackColor = True
        Me.fUseable.Visible = False
        '
        'fPotion
        '
        Me.fPotion.AutoSize = True
        Me.fPotion.Checked = True
        Me.fPotion.CheckState = System.Windows.Forms.CheckState.Checked
        Me.fPotion.Font = New System.Drawing.Font("Consolas", 8.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.fPotion.ForeColor = System.Drawing.Color.White
        Me.fPotion.Location = New System.Drawing.Point(738, 444)
        Me.fPotion.Name = "fPotion"
        Me.fPotion.Size = New System.Drawing.Size(98, 23)
        Me.fPotion.TabIndex = 237
        Me.fPotion.Text = "Potions"
        Me.fPotion.UseVisualStyleBackColor = True
        Me.fPotion.Visible = False
        '
        'fFood
        '
        Me.fFood.AutoSize = True
        Me.fFood.Checked = True
        Me.fFood.CheckState = System.Windows.Forms.CheckState.Checked
        Me.fFood.Font = New System.Drawing.Font("Consolas", 8.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.fFood.ForeColor = System.Drawing.Color.White
        Me.fFood.Location = New System.Drawing.Point(738, 466)
        Me.fFood.Name = "fFood"
        Me.fFood.Size = New System.Drawing.Size(71, 23)
        Me.fFood.TabIndex = 238
        Me.fFood.Text = "Food"
        Me.fFood.UseVisualStyleBackColor = True
        Me.fFood.Visible = False
        '
        'fArmor
        '
        Me.fArmor.AutoSize = True
        Me.fArmor.Checked = True
        Me.fArmor.CheckState = System.Windows.Forms.CheckState.Checked
        Me.fArmor.Font = New System.Drawing.Font("Consolas", 8.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.fArmor.ForeColor = System.Drawing.Color.White
        Me.fArmor.Location = New System.Drawing.Point(738, 488)
        Me.fArmor.Name = "fArmor"
        Me.fArmor.Size = New System.Drawing.Size(80, 23)
        Me.fArmor.TabIndex = 239
        Me.fArmor.Text = "Armor"
        Me.fArmor.UseVisualStyleBackColor = True
        Me.fArmor.Visible = False
        '
        'fWeapon
        '
        Me.fWeapon.AutoSize = True
        Me.fWeapon.Checked = True
        Me.fWeapon.CheckState = System.Windows.Forms.CheckState.Checked
        Me.fWeapon.Font = New System.Drawing.Font("Consolas", 8.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.fWeapon.ForeColor = System.Drawing.Color.White
        Me.fWeapon.Location = New System.Drawing.Point(738, 510)
        Me.fWeapon.Name = "fWeapon"
        Me.fWeapon.Size = New System.Drawing.Size(89, 23)
        Me.fWeapon.TabIndex = 240
        Me.fWeapon.Text = "Weapon"
        Me.fWeapon.UseVisualStyleBackColor = True
        Me.fWeapon.Visible = False
        '
        'fMisc
        '
        Me.fMisc.AutoSize = True
        Me.fMisc.Checked = True
        Me.fMisc.CheckState = System.Windows.Forms.CheckState.Checked
        Me.fMisc.Font = New System.Drawing.Font("Consolas", 8.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.fMisc.ForeColor = System.Drawing.Color.White
        Me.fMisc.Location = New System.Drawing.Point(738, 532)
        Me.fMisc.Name = "fMisc"
        Me.fMisc.Size = New System.Drawing.Size(71, 23)
        Me.fMisc.TabIndex = 241
        Me.fMisc.Text = "Misc"
        Me.fMisc.UseVisualStyleBackColor = True
        Me.fMisc.Visible = False
        '
        'btnAll
        '
        Me.btnAll.BackColor = System.Drawing.SystemColors.Window
        Me.btnAll.Font = New System.Drawing.Font("Consolas", 8.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.btnAll.Location = New System.Drawing.Point(879, 444)
        Me.btnAll.Name = "btnAll"
        Me.btnAll.Size = New System.Drawing.Size(62, 29)
        Me.btnAll.TabIndex = 242
        Me.btnAll.Text = "All"
        Me.btnAll.UseVisualStyleBackColor = False
        Me.btnAll.Visible = False
        '
        'btnNone
        '
        Me.btnNone.BackColor = System.Drawing.SystemColors.Window
        Me.btnNone.Font = New System.Drawing.Font("Consolas", 8.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.btnNone.Location = New System.Drawing.Point(879, 482)
        Me.btnNone.Name = "btnNone"
        Me.btnNone.Size = New System.Drawing.Size(62, 29)
        Me.btnNone.TabIndex = 243
        Me.btnNone.Text = "None"
        Me.btnNone.UseVisualStyleBackColor = False
        Me.btnNone.Visible = False
        '
        'picTrap
        '
        Me.picTrap.BackgroundImage = CType(resources.GetObject("picTrap.BackgroundImage"), System.Drawing.Image)
        Me.picTrap.Location = New System.Drawing.Point(582, 50)
        Me.picTrap.Name = "picTrap"
        Me.picTrap.Size = New System.Drawing.Size(15, 15)
        Me.picTrap.TabIndex = 244
        Me.picTrap.TabStop = False
        Me.picTrap.Visible = False
        '
        'btnSettings
        '
        Me.btnSettings.BackColor = System.Drawing.Color.Black
        Me.btnSettings.Cursor = System.Windows.Forms.Cursors.Hand
        Me.btnSettings.Font = New System.Drawing.Font("Consolas", 8.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.btnSettings.ForeColor = System.Drawing.Color.White
        Me.btnSettings.Location = New System.Drawing.Point(549, 403)
        Me.btnSettings.Name = "btnSettings"
        Me.btnSettings.Size = New System.Drawing.Size(99, 39)
        Me.btnSettings.TabIndex = 246
        Me.btnSettings.Text = "Settings"
        Me.btnSettings.UseVisualStyleBackColor = False
        Me.btnSettings.Visible = False
        '
        'btnT
        '
        Me.btnT.BackColor = System.Drawing.Color.Black
        Me.btnT.Cursor = System.Windows.Forms.Cursors.Hand
        Me.btnT.Font = New System.Drawing.Font("Consolas", 8.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.btnT.ForeColor = System.Drawing.Color.White
        Me.btnT.Location = New System.Drawing.Point(441, 403)
        Me.btnT.Name = "btnT"
        Me.btnT.Size = New System.Drawing.Size(99, 39)
        Me.btnT.TabIndex = 247
        Me.btnT.Text = "Tutorial"
        Me.btnT.UseVisualStyleBackColor = False
        Me.btnT.Visible = False
        '
        'Button1
        '
        Me.Button1.BackColor = System.Drawing.Color.Black
        Me.Button1.Cursor = System.Windows.Forms.Cursors.Hand
        Me.Button1.Font = New System.Drawing.Font("Consolas", 8.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Button1.ForeColor = System.Drawing.Color.White
        Me.Button1.Location = New System.Drawing.Point(335, 444)
        Me.Button1.Name = "Button1"
        Me.Button1.Size = New System.Drawing.Size(313, 39)
        Me.Button1.TabIndex = 248
        Me.Button1.Text = "Custom Content"
        Me.Button1.UseVisualStyleBackColor = False
        Me.Button1.Visible = False
        '
        'btnS8
        '
        Me.btnS8.BackColor = System.Drawing.Color.Black
        Me.btnS8.BackgroundImage = CType(resources.GetObject("btnS8.BackgroundImage"), System.Drawing.Image)
        Me.btnS8.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Stretch
        Me.btnS8.Font = New System.Drawing.Font("Consolas", 8.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.btnS8.ForeColor = System.Drawing.Color.White
        Me.btnS8.Location = New System.Drawing.Point(438, 208)
        Me.btnS8.Name = "btnS8"
        Me.btnS8.Size = New System.Drawing.Size(125, 189)
        Me.btnS8.TabIndex = 257
        Me.btnS8.UseVisualStyleBackColor = False
        '
        'btnCancel
        '
        Me.btnCancel.BackColor = System.Drawing.Color.Black
        Me.btnCancel.Font = New System.Drawing.Font("Consolas", 10.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.btnCancel.ForeColor = System.Drawing.Color.White
        Me.btnCancel.Location = New System.Drawing.Point(150, 412)
        Me.btnCancel.Name = "btnCancel"
        Me.btnCancel.Size = New System.Drawing.Size(272, 37)
        Me.btnCancel.TabIndex = 256
        Me.btnCancel.Text = "Cancel"
        Me.btnCancel.UseVisualStyleBackColor = False
        '
        'btnS7
        '
        Me.btnS7.BackColor = System.Drawing.Color.Black
        Me.btnS7.BackgroundImage = CType(resources.GetObject("btnS7.BackgroundImage"), System.Drawing.Image)
        Me.btnS7.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Stretch
        Me.btnS7.Font = New System.Drawing.Font("Consolas", 8.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.btnS7.ForeColor = System.Drawing.Color.White
        Me.btnS7.Location = New System.Drawing.Point(295, 208)
        Me.btnS7.Name = "btnS7"
        Me.btnS7.Size = New System.Drawing.Size(125, 189)
        Me.btnS7.TabIndex = 255
        Me.btnS7.UseVisualStyleBackColor = False
        '
        'btnS6
        '
        Me.btnS6.BackColor = System.Drawing.Color.Black
        Me.btnS6.BackgroundImage = CType(resources.GetObject("btnS6.BackgroundImage"), System.Drawing.Image)
        Me.btnS6.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Stretch
        Me.btnS6.Font = New System.Drawing.Font("Consolas", 8.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.btnS6.ForeColor = System.Drawing.Color.White
        Me.btnS6.Location = New System.Drawing.Point(150, 208)
        Me.btnS6.Name = "btnS6"
        Me.btnS6.Size = New System.Drawing.Size(125, 189)
        Me.btnS6.TabIndex = 254
        Me.btnS6.UseVisualStyleBackColor = False
        '
        'btnS5
        '
        Me.btnS5.BackColor = System.Drawing.Color.Black
        Me.btnS5.BackgroundImage = CType(resources.GetObject("btnS5.BackgroundImage"), System.Drawing.Image)
        Me.btnS5.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Stretch
        Me.btnS5.Font = New System.Drawing.Font("Consolas", 8.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.btnS5.ForeColor = System.Drawing.Color.White
        Me.btnS5.Location = New System.Drawing.Point(8, 208)
        Me.btnS5.Name = "btnS5"
        Me.btnS5.Size = New System.Drawing.Size(125, 189)
        Me.btnS5.TabIndex = 253
        Me.btnS5.UseVisualStyleBackColor = False
        '
        'btnS4
        '
        Me.btnS4.BackColor = System.Drawing.Color.Black
        Me.btnS4.BackgroundImage = CType(resources.GetObject("btnS4.BackgroundImage"), System.Drawing.Image)
        Me.btnS4.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Stretch
        Me.btnS4.Font = New System.Drawing.Font("Consolas", 8.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.btnS4.ForeColor = System.Drawing.Color.White
        Me.btnS4.Location = New System.Drawing.Point(438, 5)
        Me.btnS4.Name = "btnS4"
        Me.btnS4.Size = New System.Drawing.Size(125, 189)
        Me.btnS4.TabIndex = 252
        Me.btnS4.UseVisualStyleBackColor = False
        '
        'btnS3
        '
        Me.btnS3.BackColor = System.Drawing.Color.Black
        Me.btnS3.BackgroundImage = CType(resources.GetObject("btnS3.BackgroundImage"), System.Drawing.Image)
        Me.btnS3.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Stretch
        Me.btnS3.Font = New System.Drawing.Font("Consolas", 8.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.btnS3.ForeColor = System.Drawing.Color.White
        Me.btnS3.Location = New System.Drawing.Point(295, 5)
        Me.btnS3.Name = "btnS3"
        Me.btnS3.Size = New System.Drawing.Size(125, 189)
        Me.btnS3.TabIndex = 251
        Me.btnS3.UseVisualStyleBackColor = False
        '
        'btnS2
        '
        Me.btnS2.BackColor = System.Drawing.Color.Black
        Me.btnS2.BackgroundImage = CType(resources.GetObject("btnS2.BackgroundImage"), System.Drawing.Image)
        Me.btnS2.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Stretch
        Me.btnS2.Font = New System.Drawing.Font("Consolas", 8.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.btnS2.ForeColor = System.Drawing.Color.White
        Me.btnS2.Location = New System.Drawing.Point(150, 5)
        Me.btnS2.Name = "btnS2"
        Me.btnS2.Size = New System.Drawing.Size(125, 189)
        Me.btnS2.TabIndex = 250
        Me.btnS2.UseVisualStyleBackColor = False
        '
        'btnS1
        '
        Me.btnS1.BackColor = System.Drawing.Color.Black
        Me.btnS1.BackgroundImage = CType(resources.GetObject("btnS1.BackgroundImage"), System.Drawing.Image)
        Me.btnS1.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Stretch
        Me.btnS1.Font = New System.Drawing.Font("Consolas", 8.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.btnS1.ForeColor = System.Drawing.Color.White
        Me.btnS1.Location = New System.Drawing.Point(8, 5)
        Me.btnS1.Name = "btnS1"
        Me.btnS1.Size = New System.Drawing.Size(125, 189)
        Me.btnS1.TabIndex = 249
        Me.btnS1.UseVisualStyleBackColor = False
        '
        'pnlSaveLoad
        '
        Me.pnlSaveLoad.Controls.Add(Me.btnS1)
        Me.pnlSaveLoad.Controls.Add(Me.btnCancel)
        Me.pnlSaveLoad.Controls.Add(Me.btnS8)
        Me.pnlSaveLoad.Controls.Add(Me.btnS7)
        Me.pnlSaveLoad.Controls.Add(Me.btnS2)
        Me.pnlSaveLoad.Controls.Add(Me.btnS6)
        Me.pnlSaveLoad.Controls.Add(Me.btnS3)
        Me.pnlSaveLoad.Controls.Add(Me.btnS5)
        Me.pnlSaveLoad.Controls.Add(Me.btnS4)
        Me.pnlSaveLoad.Location = New System.Drawing.Point(188, 143)
        Me.pnlSaveLoad.Name = "pnlSaveLoad"
        Me.pnlSaveLoad.Size = New System.Drawing.Size(568, 458)
        Me.pnlSaveLoad.TabIndex = 258
        Me.pnlSaveLoad.Visible = False
        '
        'picSheep
        '
        Me.picSheep.BackgroundImage = CType(resources.GetObject("picSheep.BackgroundImage"), System.Drawing.Image)
        Me.picSheep.Location = New System.Drawing.Point(254, 50)
        Me.picSheep.Name = "picSheep"
        Me.picSheep.Size = New System.Drawing.Size(15, 15)
        Me.picSheep.TabIndex = 259
        Me.picSheep.TabStop = False
        Me.picSheep.Visible = False
        '
        'picFrog
        '
        Me.picFrog.BackgroundImage = CType(resources.GetObject("picFrog.BackgroundImage"), System.Drawing.Image)
        Me.picFrog.Location = New System.Drawing.Point(275, 50)
        Me.picFrog.Name = "picFrog"
        Me.picFrog.Size = New System.Drawing.Size(15, 15)
        Me.picFrog.TabIndex = 260
        Me.picFrog.TabStop = False
        Me.picFrog.Visible = False
        '
        'picTileF
        '
        Me.picTileF.BackgroundImage = CType(resources.GetObject("picTileF.BackgroundImage"), System.Drawing.Image)
        Me.picTileF.Location = New System.Drawing.Point(497, 71)
        Me.picTileF.Name = "picTileF"
        Me.picTileF.Size = New System.Drawing.Size(15, 15)
        Me.picTileF.TabIndex = 261
        Me.picTileF.TabStop = False
        Me.picTileF.Visible = False
        '
        'picTree
        '
        Me.picTree.BackgroundImage = CType(resources.GetObject("picTree.BackgroundImage"), System.Drawing.Image)
        Me.picTree.Location = New System.Drawing.Point(518, 72)
        Me.picTree.Name = "picTree"
        Me.picTree.Size = New System.Drawing.Size(15, 15)
        Me.picTree.TabIndex = 262
        Me.picTree.TabStop = False
        Me.picTree.Visible = False
        '
        'picPlayerf
        '
        Me.picPlayerf.BackgroundImage = CType(resources.GetObject("picPlayerf.BackgroundImage"), System.Drawing.Image)
        Me.picPlayerf.Location = New System.Drawing.Point(539, 71)
        Me.picPlayerf.Name = "picPlayerf"
        Me.picPlayerf.Size = New System.Drawing.Size(15, 15)
        Me.picPlayerf.TabIndex = 263
        Me.picPlayerf.TabStop = False
        Me.picPlayerf.Visible = False
        '
        'picLadderf
        '
        Me.picLadderf.BackgroundImage = CType(resources.GetObject("picLadderf.BackgroundImage"), System.Drawing.Image)
        Me.picLadderf.Location = New System.Drawing.Point(562, 71)
        Me.picLadderf.Name = "picLadderf"
        Me.picLadderf.Size = New System.Drawing.Size(15, 15)
        Me.picLadderf.TabIndex = 264
        Me.picLadderf.TabStop = False
        Me.picLadderf.Visible = False
        '
        'picChestf
        '
        Me.picChestf.BackgroundImage = CType(resources.GetObject("picChestf.BackgroundImage"), System.Drawing.Image)
        Me.picChestf.Location = New System.Drawing.Point(581, 71)
        Me.picChestf.Name = "picChestf"
        Me.picChestf.Size = New System.Drawing.Size(15, 15)
        Me.picChestf.TabIndex = 265
        Me.picChestf.TabStop = False
        Me.picChestf.Visible = False
        '
        'picBimbof
        '
        Me.picBimbof.BackgroundImage = CType(resources.GetObject("picBimbof.BackgroundImage"), System.Drawing.Image)
        Me.picBimbof.Location = New System.Drawing.Point(497, 92)
        Me.picBimbof.Name = "picBimbof"
        Me.picBimbof.Size = New System.Drawing.Size(15, 15)
        Me.picBimbof.TabIndex = 266
        Me.picBimbof.TabStop = False
        Me.picBimbof.Visible = False
        '
        'picShopkeeperf
        '
        Me.picShopkeeperf.BackgroundImage = CType(resources.GetObject("picShopkeeperf.BackgroundImage"), System.Drawing.Image)
        Me.picShopkeeperf.Location = New System.Drawing.Point(518, 93)
        Me.picShopkeeperf.Name = "picShopkeeperf"
        Me.picShopkeeperf.Size = New System.Drawing.Size(15, 15)
        Me.picShopkeeperf.TabIndex = 267
        Me.picShopkeeperf.TabStop = False
        Me.picShopkeeperf.Visible = False
        '
        'picStatuef
        '
        Me.picStatuef.BackgroundImage = CType(resources.GetObject("picStatuef.BackgroundImage"), System.Drawing.Image)
        Me.picStatuef.Location = New System.Drawing.Point(539, 91)
        Me.picStatuef.Name = "picStatuef"
        Me.picStatuef.Size = New System.Drawing.Size(15, 15)
        Me.picStatuef.TabIndex = 268
        Me.picStatuef.TabStop = False
        Me.picStatuef.Visible = False
        '
        'picTrapf
        '
        Me.picTrapf.BackgroundImage = CType(resources.GetObject("picTrapf.BackgroundImage"), System.Drawing.Image)
        Me.picTrapf.Location = New System.Drawing.Point(560, 93)
        Me.picTrapf.Name = "picTrapf"
        Me.picTrapf.Size = New System.Drawing.Size(15, 15)
        Me.picTrapf.TabIndex = 269
        Me.picTrapf.TabStop = False
        Me.picTrapf.Visible = False
        '
        'tmrKeyCD
        '
        Me.tmrKeyCD.Interval = 40
        '
        'pnlCombat
        '
        Me.pnlCombat.BackColor = System.Drawing.Color.Black
        Me.pnlCombat.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle
        Me.pnlCombat.Controls.Add(Me.lblCombatEvents)
        Me.pnlCombat.Controls.Add(Me.Label2)
        Me.pnlCombat.Controls.Add(Me.lblPHealtDiff)
        Me.pnlCombat.Controls.Add(Me.lblEHealthChange)
        Me.pnlCombat.Controls.Add(Me.lblTurn)
        Me.pnlCombat.Controls.Add(Me.lblPHealth)
        Me.pnlCombat.Controls.Add(Me.lblPName)
        Me.pnlCombat.Controls.Add(Me.lblEHealth)
        Me.pnlCombat.Controls.Add(Me.lblEName)
        Me.pnlCombat.Controls.Add(Me.picPHealth)
        Me.pnlCombat.Controls.Add(Me.picEHbar)
        Me.pnlCombat.Location = New System.Drawing.Point(115, 50)
        Me.pnlCombat.Name = "pnlCombat"
        Me.pnlCombat.Size = New System.Drawing.Size(568, 362)
        Me.pnlCombat.TabIndex = 270
        Me.pnlCombat.Visible = False
        '
        'lblCombatEvents
        '
        Me.lblCombatEvents.BackColor = System.Drawing.Color.Black
        Me.lblCombatEvents.BorderStyle = System.Windows.Forms.BorderStyle.None
        Me.lblCombatEvents.Cursor = System.Windows.Forms.Cursors.Arrow
        Me.lblCombatEvents.Font = New System.Drawing.Font("Consolas", 9.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.lblCombatEvents.ForeColor = System.Drawing.Color.White
        Me.lblCombatEvents.Location = New System.Drawing.Point(9, 126)
        Me.lblCombatEvents.Multiline = True
        Me.lblCombatEvents.Name = "lblCombatEvents"
        Me.lblCombatEvents.ReadOnly = True
        Me.lblCombatEvents.ScrollBars = System.Windows.Forms.ScrollBars.Vertical
        Me.lblCombatEvents.Size = New System.Drawing.Size(549, 205)
        Me.lblCombatEvents.TabIndex = 11
        '
        'Label2
        '
        Me.Label2.AutoSize = True
        Me.Label2.Font = New System.Drawing.Font("Consolas", 9.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Label2.ForeColor = System.Drawing.Color.White
        Me.Label2.Location = New System.Drawing.Point(5, 334)
        Me.Label2.Name = "Label2"
        Me.Label2.Size = New System.Drawing.Size(400, 22)
        Me.Label2.TabIndex = 10
        Me.Label2.Text = "Press a combat button to continue . . ."
        '
        'lblPHealtDiff
        '
        Me.lblPHealtDiff.AutoSize = True
        Me.lblPHealtDiff.Font = New System.Drawing.Font("Consolas", 9.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.lblPHealtDiff.ForeColor = System.Drawing.Color.White
        Me.lblPHealtDiff.Location = New System.Drawing.Point(379, 88)
        Me.lblPHealtDiff.Name = "lblPHealtDiff"
        Me.lblPHealtDiff.Size = New System.Drawing.Size(50, 22)
        Me.lblPHealtDiff.TabIndex = 8
        Me.lblPHealtDiff.Text = "-999"
        '
        'lblEHealthChange
        '
        Me.lblEHealthChange.AutoSize = True
        Me.lblEHealthChange.Font = New System.Drawing.Font("Consolas", 9.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.lblEHealthChange.ForeColor = System.Drawing.Color.White
        Me.lblEHealthChange.Location = New System.Drawing.Point(139, 91)
        Me.lblEHealthChange.Name = "lblEHealthChange"
        Me.lblEHealthChange.Size = New System.Drawing.Size(50, 22)
        Me.lblEHealthChange.TabIndex = 7
        Me.lblEHealthChange.Text = "-999"
        '
        'lblTurn
        '
        Me.lblTurn.AutoSize = True
        Me.lblTurn.Font = New System.Drawing.Font("Consolas", 9.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.lblTurn.ForeColor = System.Drawing.Color.White
        Me.lblTurn.Location = New System.Drawing.Point(236, 20)
        Me.lblTurn.Name = "lblTurn"
        Me.lblTurn.Size = New System.Drawing.Size(120, 22)
        Me.lblTurn.TabIndex = 6
        Me.lblTurn.Text = "Turn: 99999"
        '
        'lblPHealth
        '
        Me.lblPHealth.AutoSize = True
        Me.lblPHealth.Font = New System.Drawing.Font("Consolas", 9.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.lblPHealth.ForeColor = System.Drawing.Color.White
        Me.lblPHealth.Location = New System.Drawing.Point(450, 48)
        Me.lblPHealth.Name = "lblPHealth"
        Me.lblPHealth.Size = New System.Drawing.Size(100, 22)
        Me.lblPHealth.TabIndex = 5
        Me.lblPHealth.Text = "9999/9999"
        '
        'lblPName
        '
        Me.lblPName.AutoSize = True
        Me.lblPName.BackColor = System.Drawing.Color.Black
        Me.lblPName.Font = New System.Drawing.Font("Consolas", 9.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.lblPName.ForeColor = System.Drawing.Color.White
        Me.lblPName.Location = New System.Drawing.Point(448, 19)
        Me.lblPName.Name = "lblPName"
        Me.lblPName.Size = New System.Drawing.Size(110, 22)
        Me.lblPName.TabIndex = 4
        Me.lblPName.Text = "PlayerName"
        '
        'lblEHealth
        '
        Me.lblEHealth.AutoSize = True
        Me.lblEHealth.Font = New System.Drawing.Font("Consolas", 9.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.lblEHealth.ForeColor = System.Drawing.Color.White
        Me.lblEHealth.Location = New System.Drawing.Point(20, 49)
        Me.lblEHealth.Name = "lblEHealth"
        Me.lblEHealth.Size = New System.Drawing.Size(100, 22)
        Me.lblEHealth.TabIndex = 3
        Me.lblEHealth.Text = "9999/9999"
        '
        'lblEName
        '
        Me.lblEName.AutoSize = True
        Me.lblEName.BackColor = System.Drawing.Color.Black
        Me.lblEName.Font = New System.Drawing.Font("Consolas", 9.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.lblEName.ForeColor = System.Drawing.Color.White
        Me.lblEName.Location = New System.Drawing.Point(18, 19)
        Me.lblEName.Name = "lblEName"
        Me.lblEName.Size = New System.Drawing.Size(100, 22)
        Me.lblEName.TabIndex = 2
        Me.lblEName.Text = "EnemyName"
        '
        'picPHealth
        '
        Me.picPHealth.BackColor = System.Drawing.Color.YellowGreen
        Me.picPHealth.Location = New System.Drawing.Point(374, 73)
        Me.picPHealth.Name = "picPHealth"
        Me.picPHealth.Size = New System.Drawing.Size(174, 15)
        Me.picPHealth.TabIndex = 1
        Me.picPHealth.TabStop = False
        '
        'picEHbar
        '
        Me.picEHbar.BackColor = System.Drawing.Color.YellowGreen
        Me.picEHbar.Location = New System.Drawing.Point(22, 73)
        Me.picEHbar.Name = "picEHbar"
        Me.picEHbar.Size = New System.Drawing.Size(174, 15)
        Me.picEHbar.TabIndex = 0
        Me.picEHbar.TabStop = False
        '
        'lblEvent
        '
        Me.lblEvent.AutoSize = True
        Me.lblEvent.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle
        Me.lblEvent.Font = New System.Drawing.Font("Consolas", 9.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.lblEvent.ForeColor = System.Drawing.Color.White
        Me.lblEvent.Location = New System.Drawing.Point(304, 120)
        Me.lblEvent.Name = "lblEvent"
        Me.lblEvent.Size = New System.Drawing.Size(72, 24)
        Me.lblEvent.TabIndex = 271
        Me.lblEvent.Text = "Label2"
        Me.lblEvent.TextAlign = System.Drawing.ContentAlignment.MiddleLeft
        Me.lblEvent.Visible = False
        '
        'picLoadBar
        '
        Me.picLoadBar.BackColor = System.Drawing.Color.White
        Me.picLoadBar.Location = New System.Drawing.Point(306, 361)
        Me.picLoadBar.Name = "picLoadBar"
        Me.picLoadBar.Size = New System.Drawing.Size(395, 17)
        Me.picLoadBar.TabIndex = 11
        Me.picLoadBar.TabStop = False
        Me.picLoadBar.Visible = False
        '
        'btnWait
        '
        Me.btnWait.BackColor = System.Drawing.Color.Black
        Me.btnWait.Font = New System.Drawing.Font("Consolas", 8.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.btnWait.ForeColor = System.Drawing.Color.White
        Me.btnWait.Location = New System.Drawing.Point(497, 419)
        Me.btnWait.Name = "btnWait"
        Me.btnWait.Size = New System.Drawing.Size(86, 36)
        Me.btnWait.TabIndex = 272
        Me.btnWait.Text = "Wait"
        Me.btnWait.UseVisualStyleBackColor = False
        Me.btnWait.Visible = False
        '
        'picSWiz
        '
        Me.picSWiz.BackgroundImage = CType(resources.GetObject("picSWiz.BackgroundImage"), System.Drawing.Image)
        Me.picSWiz.Location = New System.Drawing.Point(611, 12)
        Me.picSWiz.Name = "picSWiz"
        Me.picSWiz.Size = New System.Drawing.Size(15, 15)
        Me.picSWiz.TabIndex = 273
        Me.picSWiz.TabStop = False
        Me.picSWiz.Visible = False
        '
        'picSWizF
        '
        Me.picSWizF.BackgroundImage = CType(resources.GetObject("picSWizF.BackgroundImage"), System.Drawing.Image)
        Me.picSWizF.Location = New System.Drawing.Point(632, 12)
        Me.picSWizF.Name = "picSWizF"
        Me.picSWizF.Size = New System.Drawing.Size(15, 15)
        Me.picSWizF.TabIndex = 274
        Me.picSWizF.TabStop = False
        Me.picSWizF.Visible = False
        '
        'Game
        '
        Me.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Inherit
        Me.BackColor = System.Drawing.Color.Black
        Me.ClientSize = New System.Drawing.Size(1008, 690)
        Me.Controls.Add(Me.picSWizF)
        Me.Controls.Add(Me.picSWiz)
        Me.Controls.Add(Me.picLoadBar)
        Me.Controls.Add(Me.pnlCombat)
        Me.Controls.Add(Me.picTrapf)
        Me.Controls.Add(Me.picStatuef)
        Me.Controls.Add(Me.picShopkeeperf)
        Me.Controls.Add(Me.picBimbof)
        Me.Controls.Add(Me.picChestf)
        Me.Controls.Add(Me.picLadderf)
        Me.Controls.Add(Me.picPlayerf)
        Me.Controls.Add(Me.picTree)
        Me.Controls.Add(Me.picTileF)
        Me.Controls.Add(Me.picFrog)
        Me.Controls.Add(Me.picSheep)
        Me.Controls.Add(Me.pnlSaveLoad)
        Me.Controls.Add(Me.Button1)
        Me.Controls.Add(Me.btnT)
        Me.Controls.Add(Me.btnSettings)
        Me.Controls.Add(Me.picTrap)
        Me.Controls.Add(Me.btnNone)
        Me.Controls.Add(Me.btnAll)
        Me.Controls.Add(Me.fMisc)
        Me.Controls.Add(Me.fWeapon)
        Me.Controls.Add(Me.fArmor)
        Me.Controls.Add(Me.fFood)
        Me.Controls.Add(Me.fPotion)
        Me.Controls.Add(Me.fUseable)
        Me.Controls.Add(Me.btnOk)
        Me.Controls.Add(Me.btnSpec)
        Me.Controls.Add(Me.cmboxSpec)
        Me.Controls.Add(Me.picLust5)
        Me.Controls.Add(Me.picLust4)
        Me.Controls.Add(Me.picLust3)
        Me.Controls.Add(Me.picLust2)
        Me.Controls.Add(Me.picLust1)
        Me.Controls.Add(Me.picSKBunny)
        Me.Controls.Add(Me.picSKPrin)
        Me.Controls.Add(Me.picShopkeep)
        Me.Controls.Add(Me.picShopkeepTile)
        Me.Controls.Add(Me.picMarkBClothes)
        Me.Controls.Add(Me.picMarkBRearHair2)
        Me.Controls.Add(Me.picMarkBRearHair1)
        Me.Controls.Add(Me.picMarkBFHair)
        Me.Controls.Add(Me.btnControls)
        Me.Controls.Add(Me.btnChallengeBoss)
        Me.Controls.Add(Me.picEnemy)
        Me.Controls.Add(Me.picMarkEyebrows)
        Me.Controls.Add(Me.picFMarkBody)
        Me.Controls.Add(Me.picfMarkClothes)
        Me.Controls.Add(Me.picFMarkRHair2)
        Me.Controls.Add(Me.picFMarkRHair1)
        Me.Controls.Add(Me.picFMarkFHair)
        Me.Controls.Add(Me.picMarkClothes)
        Me.Controls.Add(Me.picMarkRHair2)
        Me.Controls.Add(Me.picMarkRHair1)
        Me.Controls.Add(Me.picMarkFHair)
        Me.Controls.Add(Me.btnLeave)
        Me.Controls.Add(Me.btnFight)
        Me.Controls.Add(Me.btnNPCMG)
        Me.Controls.Add(Me.cboxNPCMG)
        Me.Controls.Add(Me.btnShop)
        Me.Controls.Add(Me.btnTalk)
        Me.Controls.Add(Me.picNPC)
        Me.Controls.Add(Me.picStatue)
        Me.Controls.Add(Me.picChicken)
        Me.Controls.Add(Me.picPlayerB)
        Me.Controls.Add(Me.picChest)
        Me.Controls.Add(Me.picStairs)
        Me.Controls.Add(Me.picPlayer)
        Me.Controls.Add(Me.picFog)
        Me.Controls.Add(Me.picTile)
        Me.Controls.Add(Me.btnL)
        Me.Controls.Add(Me.btnS)
        Me.Controls.Add(Me.picStart)
        Me.Controls.Add(Me.btnEQP)
        Me.Controls.Add(Me.btnRUN)
        Me.Controls.Add(Me.btnMG)
        Me.Controls.Add(Me.cboxMG)
        Me.Controls.Add(Me.btnATK)
        Me.Controls.Add(Me.lblEVD)
        Me.Controls.Add(Me.lblSPD)
        Me.Controls.Add(Me.lblSKL)
        Me.Controls.Add(Me.lblDEF)
        Me.Controls.Add(Me.lblATK)
        Me.Controls.Add(Me.lblLevel)
        Me.Controls.Add(Me.lblXP)
        Me.Controls.Add(Me.lblHunger)
        Me.Controls.Add(Me.lblMana)
        Me.Controls.Add(Me.lblHealth)
        Me.Controls.Add(Me.lblNameTitle)
        Me.Controls.Add(Me.Label1)
        Me.Controls.Add(Me.btnUse)
        Me.Controls.Add(Me.lstLog)
        Me.Controls.Add(Me.MenuStrip1)
        Me.Controls.Add(Me.btnLook)
        Me.Controls.Add(Me.btnDrop)
        Me.Controls.Add(Me.btnUse1)
        Me.Controls.Add(Me.btnEXM)
        Me.Controls.Add(Me.lblGold)
        Me.Controls.Add(Me.btnIns)
        Me.Controls.Add(Me.btnR)
        Me.Controls.Add(Me.btnU)
        Me.Controls.Add(Me.BtnD)
        Me.Controls.Add(Me.btnLft)
        Me.Controls.Add(Me.btnFilter)
        Me.Controls.Add(Me.lstInventory)
        Me.Controls.Add(Me.picPortrait)
        Me.Controls.Add(Me.lblEvent)
        Me.Controls.Add(Me.btnWait)
        Me.FormBorderStyle = System.Windows.Forms.FormBorderStyle.FixedSingle
        Me.Icon = CType(resources.GetObject("$this.Icon"), System.Drawing.Icon)
        Me.MaximizeBox = False
        Me.Name = "Game"
        Me.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen
        Me.Text = "The_Dungeon"
        CType(Me.picMarkBClothes, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.picMarkBRearHair2, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.picMarkBRearHair1, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.picMarkBFHair, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.picEnemy, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.picMarkEyebrows, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.picFMarkBody, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.picfMarkClothes, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.picFMarkRHair2, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.picFMarkRHair1, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.picFMarkFHair, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.picMarkClothes, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.picMarkRHair2, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.picMarkRHair1, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.picMarkFHair, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.picNPC, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.picStatue, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.picChicken, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.picPlayerB, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.picChest, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.picStairs, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.picPlayer, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.picFog, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.picTile, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.picStart, System.ComponentModel.ISupportInitialize).EndInit()
        Me.MenuStrip1.ResumeLayout(False)
        Me.MenuStrip1.PerformLayout()
        CType(Me.picPortrait, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.picShopkeepTile, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.picShopkeep, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.picSKPrin, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.picSKBunny, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.picLust1, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.picLust2, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.picLust3, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.picLust4, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.picLust5, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.picTrap, System.ComponentModel.ISupportInitialize).EndInit()
        Me.pnlSaveLoad.ResumeLayout(False)
        CType(Me.picSheep, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.picFrog, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.picTileF, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.picTree, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.picPlayerf, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.picLadderf, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.picChestf, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.picBimbof, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.picShopkeeperf, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.picStatuef, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.picTrapf, System.ComponentModel.ISupportInitialize).EndInit()
        Me.pnlCombat.ResumeLayout(False)
        Me.pnlCombat.PerformLayout()
        CType(Me.picPHealth, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.picEHbar, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.picLoadBar, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.picSWiz, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.picSWizF, System.ComponentModel.ISupportInitialize).EndInit()
        Me.ResumeLayout(False)
        Me.PerformLayout()

    End Sub
    Friend WithEvents btnUse1 As System.Windows.Forms.Button
    Friend WithEvents btnDrop As System.Windows.Forms.Button
    Friend WithEvents btnLook As System.Windows.Forms.Button
    Friend WithEvents picMarkBClothes As System.Windows.Forms.PictureBox
    Friend WithEvents picMarkBRearHair2 As System.Windows.Forms.PictureBox
    Friend WithEvents picMarkBRearHair1 As System.Windows.Forms.PictureBox
    Friend WithEvents picMarkBFHair As System.Windows.Forms.PictureBox
    Friend WithEvents btnControls As System.Windows.Forms.Button
    Friend WithEvents btnChallengeBoss As System.Windows.Forms.Button
    Friend WithEvents picEnemy As System.Windows.Forms.PictureBox
    Friend WithEvents picMarkEyebrows As System.Windows.Forms.PictureBox
    Friend WithEvents picFMarkBody As System.Windows.Forms.PictureBox
    Friend WithEvents picfMarkClothes As System.Windows.Forms.PictureBox
    Friend WithEvents picFMarkRHair2 As System.Windows.Forms.PictureBox
    Friend WithEvents picFMarkRHair1 As System.Windows.Forms.PictureBox
    Friend WithEvents picFMarkFHair As System.Windows.Forms.PictureBox
    Friend WithEvents picMarkClothes As System.Windows.Forms.PictureBox
    Friend WithEvents picMarkRHair2 As System.Windows.Forms.PictureBox
    Friend WithEvents picMarkRHair1 As System.Windows.Forms.PictureBox
    Friend WithEvents picMarkFHair As System.Windows.Forms.PictureBox
    Friend WithEvents btnLeave As System.Windows.Forms.Button
    Friend WithEvents btnFight As System.Windows.Forms.Button
    Friend WithEvents btnNPCMG As System.Windows.Forms.Button
    Friend WithEvents cboxNPCMG As System.Windows.Forms.ComboBox
    Friend WithEvents btnShop As System.Windows.Forms.Button
    Friend WithEvents btnTalk As System.Windows.Forms.Button
    Friend WithEvents picNPC As System.Windows.Forms.PictureBox
    Friend WithEvents picStatue As System.Windows.Forms.PictureBox
    Friend WithEvents picChicken As System.Windows.Forms.PictureBox
    Friend WithEvents picPlayerB As System.Windows.Forms.PictureBox
    Friend WithEvents picChest As System.Windows.Forms.PictureBox
    Friend WithEvents picStairs As System.Windows.Forms.PictureBox
    Friend WithEvents picPlayer As System.Windows.Forms.PictureBox
    Friend WithEvents picFog As System.Windows.Forms.PictureBox
    Friend WithEvents picTile As System.Windows.Forms.PictureBox
    Friend WithEvents btnL As System.Windows.Forms.Button
    Friend WithEvents btnS As System.Windows.Forms.Button
    Friend WithEvents picStart As System.Windows.Forms.PictureBox
    Friend WithEvents btnEQP As System.Windows.Forms.Button
    Friend WithEvents btnRUN As System.Windows.Forms.Button
    Friend WithEvents btnMG As System.Windows.Forms.Button
    Friend WithEvents cboxMG As System.Windows.Forms.ComboBox
    Friend WithEvents btnATK As System.Windows.Forms.Button
    Friend WithEvents lblEVD As System.Windows.Forms.Label
    Friend WithEvents lblSPD As System.Windows.Forms.Label
    Friend WithEvents lblSKL As System.Windows.Forms.Label
    Friend WithEvents lblDEF As System.Windows.Forms.Label
    Friend WithEvents lblATK As System.Windows.Forms.Label
    Friend WithEvents lblLevel As System.Windows.Forms.Label
    Friend WithEvents lblXP As System.Windows.Forms.Label
    Friend WithEvents lblHunger As System.Windows.Forms.Label
    Friend WithEvents lblMana As System.Windows.Forms.Label
    Friend WithEvents lblHealth As System.Windows.Forms.Label
    Friend WithEvents lblNameTitle As System.Windows.Forms.Label
    Friend WithEvents Label1 As System.Windows.Forms.Label
    Friend WithEvents btnUse As System.Windows.Forms.Button
    Friend WithEvents lstInventory As System.Windows.Forms.ListBox
    Friend WithEvents lstLog As System.Windows.Forms.ListBox
    Friend WithEvents MenuStrip1 As System.Windows.Forms.MenuStrip
    Friend WithEvents FileToolStripMenuItem As System.Windows.Forms.ToolStripMenuItem
    Friend WithEvents SaveToolStripMenuItem As System.Windows.Forms.ToolStripMenuItem
    Friend WithEvents LoadToolStripMenuItem As System.Windows.Forms.ToolStripMenuItem
    Friend WithEvents NewGameToolStripMenuItem As System.Windows.Forms.ToolStripMenuItem
    Friend WithEvents HelpToolStripMenuItem As System.Windows.Forms.ToolStripMenuItem
    Friend WithEvents HelpToolStripMenuItem1 As System.Windows.Forms.ToolStripMenuItem
    Friend WithEvents WASDArrowsMoveToolStripMenuItem As System.Windows.Forms.ToolStripMenuItem
    Friend WithEvents InteractstairschestsToolStripMenuItem As System.Windows.Forms.ToolStripMenuItem
    Friend WithEvents picPortrait As System.Windows.Forms.PictureBox
    Friend WithEvents btnEXM As System.Windows.Forms.Button
    Friend WithEvents lblGold As System.Windows.Forms.Label
    Friend WithEvents picShopkeepTile As System.Windows.Forms.PictureBox
    Friend WithEvents picShopkeep As System.Windows.Forms.PictureBox
    Friend WithEvents picSKPrin As System.Windows.Forms.PictureBox
    Friend WithEvents picSKBunny As System.Windows.Forms.PictureBox
    Friend WithEvents picLust1 As System.Windows.Forms.PictureBox
    Friend WithEvents picLust2 As System.Windows.Forms.PictureBox
    Friend WithEvents picLust3 As System.Windows.Forms.PictureBox
    Friend WithEvents picLust4 As System.Windows.Forms.PictureBox
    Friend WithEvents picLust5 As System.Windows.Forms.PictureBox
    Friend WithEvents btnIns As System.Windows.Forms.Button
    Friend WithEvents btnR As System.Windows.Forms.Button
    Friend WithEvents btnU As System.Windows.Forms.Button
    Friend WithEvents BtnD As System.Windows.Forms.Button
    Friend WithEvents btnLft As System.Windows.Forms.Button
    Friend WithEvents cmboxSpec As System.Windows.Forms.ComboBox
    Friend WithEvents btnSpec As System.Windows.Forms.Button
    Friend WithEvents btnFilter As System.Windows.Forms.Button
    Friend WithEvents btnOk As System.Windows.Forms.Button
    Friend WithEvents fUseable As System.Windows.Forms.CheckBox
    Friend WithEvents fPotion As System.Windows.Forms.CheckBox
    Friend WithEvents fFood As System.Windows.Forms.CheckBox
    Friend WithEvents fArmor As System.Windows.Forms.CheckBox
    Friend WithEvents fWeapon As System.Windows.Forms.CheckBox
    Friend WithEvents fMisc As System.Windows.Forms.CheckBox
    Friend WithEvents btnAll As System.Windows.Forms.Button
    Friend WithEvents btnNone As System.Windows.Forms.Button
    Friend WithEvents picTrap As System.Windows.Forms.PictureBox
    Friend WithEvents StatInfoToolStripMenuItem As System.Windows.Forms.ToolStripMenuItem
    Friend WithEvents InfoToolStripMenuItem As System.Windows.Forms.ToolStripMenuItem
    Friend WithEvents SettingsToolStripMenuItem As System.Windows.Forms.ToolStripMenuItem
    Friend WithEvents btnSettings As System.Windows.Forms.Button
    Friend WithEvents btnT As System.Windows.Forms.Button
    Friend WithEvents Button1 As System.Windows.Forms.Button
    Friend WithEvents btnS8 As System.Windows.Forms.Button
    Friend WithEvents btnCancel As System.Windows.Forms.Button
    Friend WithEvents btnS7 As System.Windows.Forms.Button
    Friend WithEvents btnS6 As System.Windows.Forms.Button
    Friend WithEvents btnS5 As System.Windows.Forms.Button
    Friend WithEvents btnS4 As System.Windows.Forms.Button
    Friend WithEvents btnS3 As System.Windows.Forms.Button
    Friend WithEvents btnS2 As System.Windows.Forms.Button
    Friend WithEvents btnS1 As System.Windows.Forms.Button
    Friend WithEvents pnlSaveLoad As System.Windows.Forms.Panel
    Friend WithEvents picSheep As System.Windows.Forms.PictureBox
    Friend WithEvents picFrog As System.Windows.Forms.PictureBox
    Friend WithEvents picTileF As System.Windows.Forms.PictureBox
    Friend WithEvents picTree As System.Windows.Forms.PictureBox
    Friend WithEvents picPlayerf As System.Windows.Forms.PictureBox
    Friend WithEvents picLadderf As System.Windows.Forms.PictureBox
    Friend WithEvents picChestf As System.Windows.Forms.PictureBox
    Friend WithEvents picBimbof As System.Windows.Forms.PictureBox
    Friend WithEvents picShopkeeperf As System.Windows.Forms.PictureBox
    Friend WithEvents picStatuef As System.Windows.Forms.PictureBox
    Friend WithEvents picTrapf As System.Windows.Forms.PictureBox
    Friend WithEvents tmrKeyCD As System.Windows.Forms.Timer
    Friend WithEvents ReportToolStripMenuItem As System.Windows.Forms.ToolStripMenuItem
    Friend WithEvents pnlCombat As System.Windows.Forms.Panel
    Friend WithEvents lblPHealtDiff As System.Windows.Forms.Label
    Friend WithEvents lblEHealthChange As System.Windows.Forms.Label
    Friend WithEvents lblTurn As System.Windows.Forms.Label
    Friend WithEvents lblPHealth As System.Windows.Forms.Label
    Friend WithEvents lblPName As System.Windows.Forms.Label
    Friend WithEvents lblEHealth As System.Windows.Forms.Label
    Friend WithEvents lblEName As System.Windows.Forms.Label
    Friend WithEvents picPHealth As System.Windows.Forms.PictureBox
    Friend WithEvents picEHbar As System.Windows.Forms.PictureBox
    Friend WithEvents lblEvent As System.Windows.Forms.Label
    Friend WithEvents Label2 As System.Windows.Forms.Label
    Friend WithEvents DebugToolStripMenuItem As ToolStripMenuItem
    Friend WithEvents picLoadBar As System.Windows.Forms.PictureBox
    Friend WithEvents lblCombatEvents As System.Windows.Forms.TextBox
    Friend WithEvents btnWait As System.Windows.Forms.Button
    Friend WithEvents picSWiz As System.Windows.Forms.PictureBox
    Friend WithEvents picSWizF As System.Windows.Forms.PictureBox
End Class
