﻿<Global.Microsoft.VisualBasic.CompilerServices.DesignerGenerated()>
Partial Class Debug_Window
    Inherits System.Windows.Forms.Form

    'Form overrides dispose to clean up the component list.
    <System.Diagnostics.DebuggerNonUserCode()>
    Protected Overrides Sub Dispose(ByVal disposing As Boolean)
        Try
            If disposing AndAlso components IsNot Nothing Then
                components.Dispose()
            End If
        Finally
            MyBase.Dispose(disposing)
        End Try
    End Sub

    'Required by the Windows Form Designer
    Private components As System.ComponentModel.IContainer

    'NOTE: The following procedure is required by the Windows Form Designer
    'It can be modified using the Windows Form Designer.  
    'Do not modify it using the code editor.
    <System.Diagnostics.DebuggerStepThrough()>
    Private Sub InitializeComponent()
        Dim resources As System.ComponentModel.ComponentResourceManager = New System.ComponentModel.ComponentResourceManager(GetType(Debug_Window))
        Me.tabMain = New System.Windows.Forms.TabControl()
        Me.tabInformation = New System.Windows.Forms.TabPage()
        Me.boxNotes = New System.Windows.Forms.RichTextBox()
        Me.tabGeneral = New System.Windows.Forms.TabPage()
        Me.groupNotes = New System.Windows.Forms.GroupBox()
        Me.groupGeneral = New System.Windows.Forms.GroupBox()
        Me.boxTurn = New System.Windows.Forms.NumericUpDown()
        Me.boxFloor = New System.Windows.Forms.NumericUpDown()
        Me.lblTurn = New System.Windows.Forms.Label()
        Me.lblFloor = New System.Windows.Forms.Label()
        Me.tabPlayer = New System.Windows.Forms.TabPage()
        Me.lblHC = New System.Windows.Forms.Label()
        Me.pnlHC = New System.Windows.Forms.Panel()
        Me.lblSC = New System.Windows.Forms.Label()
        Me.pnlSC = New System.Windows.Forms.Panel()
        Me.tabPortrait = New System.Windows.Forms.TabControl()
        Me.tabPageBackground = New System.Windows.Forms.TabPage()
        Me.tabPageRearHair = New System.Windows.Forms.TabPage()
        Me.tabPageBody = New System.Windows.Forms.TabPage()
        Me.tabPageClothing = New System.Windows.Forms.TabPage()
        Me.tabPageFace = New System.Windows.Forms.TabPage()
        Me.tabPageMiddleHair = New System.Windows.Forms.TabPage()
        Me.tabPageEars = New System.Windows.Forms.TabPage()
        Me.tabPageNose = New System.Windows.Forms.TabPage()
        Me.tabPageMouth = New System.Windows.Forms.TabPage()
        Me.tabPageEyes = New System.Windows.Forms.TabPage()
        Me.tabPageEyebrows = New System.Windows.Forms.TabPage()
        Me.tabPageFaceMark = New System.Windows.Forms.TabPage()
        Me.tabPageGlasses = New System.Windows.Forms.TabPage()
        Me.tabPageCloak = New System.Windows.Forms.TabPage()
        Me.tabPageAccessories = New System.Windows.Forms.TabPage()
        Me.tabPageFrontHair = New System.Windows.Forms.TabPage()
        Me.tabPageHat = New System.Windows.Forms.TabPage()
        Me.picPreview = New System.Windows.Forms.PictureBox()
        Me.playerDivider = New System.Windows.Forms.TextBox()
        Me.lblGold = New System.Windows.Forms.Label()
        Me.boxEvd = New System.Windows.Forms.NumericUpDown()
        Me.lblEvd = New System.Windows.Forms.Label()
        Me.boxSpd = New System.Windows.Forms.NumericUpDown()
        Me.lblSpd = New System.Windows.Forms.Label()
        Me.boxWil = New System.Windows.Forms.NumericUpDown()
        Me.lblWil = New System.Windows.Forms.Label()
        Me.boxDef = New System.Windows.Forms.NumericUpDown()
        Me.lblDef = New System.Windows.Forms.Label()
        Me.boxAtk = New System.Windows.Forms.NumericUpDown()
        Me.lblAtk = New System.Windows.Forms.Label()
        Me.boxGold = New System.Windows.Forms.NumericUpDown()
        Me.boxHunger = New System.Windows.Forms.NumericUpDown()
        Me.lblHunger = New System.Windows.Forms.Label()
        Me.boxMaxMana = New System.Windows.Forms.NumericUpDown()
        Me.lblOf2 = New System.Windows.Forms.Label()
        Me.boxMana = New System.Windows.Forms.NumericUpDown()
        Me.lblMana = New System.Windows.Forms.Label()
        Me.boxMaxHealth = New System.Windows.Forms.NumericUpDown()
        Me.lblOf1 = New System.Windows.Forms.Label()
        Me.boxHealth = New System.Windows.Forms.NumericUpDown()
        Me.lblHealth = New System.Windows.Forms.Label()
        Me.boxForm = New System.Windows.Forms.ComboBox()
        Me.boxName = New System.Windows.Forms.TextBox()
        Me.boxSex = New System.Windows.Forms.ComboBox()
        Me.lblTitle = New System.Windows.Forms.Label()
        Me.lblSex = New System.Windows.Forms.Label()
        Me.lblName = New System.Windows.Forms.Label()
        Me.tabInventory = New System.Windows.Forms.TabPage()
        Me.number = New System.Windows.Forms.NumericUpDown()
        Me.btnRemove = New System.Windows.Forms.Button()
        Me.boxInventory = New System.Windows.Forms.ListBox()
        Me.btnAdd = New System.Windows.Forms.Button()
        Me.lblInventory = New System.Windows.Forms.Label()
        Me.boxItems = New System.Windows.Forms.ListBox()
        Me.lblItems = New System.Windows.Forms.Label()
        Me.picBoard = New System.Windows.Forms.PictureBox()
        Me.tabMain.SuspendLayout()
        Me.tabInformation.SuspendLayout()
        Me.tabGeneral.SuspendLayout()
        Me.groupNotes.SuspendLayout()
        Me.groupGeneral.SuspendLayout()
        CType(Me.boxTurn, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.boxFloor, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.tabPlayer.SuspendLayout()
        Me.tabPortrait.SuspendLayout()
        CType(Me.picPreview, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.boxEvd, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.boxSpd, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.boxWil, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.boxDef, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.boxAtk, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.boxGold, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.boxHunger, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.boxMaxMana, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.boxMana, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.boxMaxHealth, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.boxHealth, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.tabInventory.SuspendLayout()
        CType(Me.number, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.picBoard, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.SuspendLayout()
        '
        'tabMain
        '
        Me.tabMain.Controls.Add(Me.tabInformation)
        Me.tabMain.Controls.Add(Me.tabGeneral)
        Me.tabMain.Controls.Add(Me.tabPlayer)
        Me.tabMain.Controls.Add(Me.tabInventory)
        Me.tabMain.Dock = System.Windows.Forms.DockStyle.Fill
        Me.tabMain.Font = New System.Drawing.Font("Consolas", 10.0!)
        Me.tabMain.Location = New System.Drawing.Point(0, 0)
        Me.tabMain.Name = "tabMain"
        Me.tabMain.SelectedIndex = 0
        Me.tabMain.Size = New System.Drawing.Size(750, 576)
        Me.tabMain.TabIndex = 207
        '
        'tabInformation
        '
        Me.tabInformation.BackColor = System.Drawing.Color.Black
        Me.tabInformation.Controls.Add(Me.boxNotes)
        Me.tabInformation.ForeColor = System.Drawing.Color.White
        Me.tabInformation.Location = New System.Drawing.Point(4, 24)
        Me.tabInformation.Name = "tabInformation"
        Me.tabInformation.Padding = New System.Windows.Forms.Padding(3)
        Me.tabInformation.Size = New System.Drawing.Size(742, 548)
        Me.tabInformation.TabIndex = 3
        Me.tabInformation.Text = "INFORMATION"
        '
        'boxNotes
        '
        Me.boxNotes.BackColor = System.Drawing.Color.Black
        Me.boxNotes.ForeColor = System.Drawing.Color.White
        Me.boxNotes.Location = New System.Drawing.Point(135, 55)
        Me.boxNotes.Name = "boxNotes"
        Me.boxNotes.ReadOnly = True
        Me.boxNotes.Size = New System.Drawing.Size(472, 439)
        Me.boxNotes.TabIndex = 2
        Me.boxNotes.Text = resources.GetString("boxNotes.Text")
        '
        'tabGeneral
        '
        Me.tabGeneral.BackColor = System.Drawing.Color.Black
        Me.tabGeneral.Controls.Add(Me.groupNotes)
        Me.tabGeneral.Controls.Add(Me.groupGeneral)
        Me.tabGeneral.Location = New System.Drawing.Point(4, 24)
        Me.tabGeneral.Name = "tabGeneral"
        Me.tabGeneral.Padding = New System.Windows.Forms.Padding(3)
        Me.tabGeneral.Size = New System.Drawing.Size(742, 548)
        Me.tabGeneral.TabIndex = 0
        Me.tabGeneral.Text = "GENERAL"
        '
        'groupNotes
        '
        Me.groupNotes.Controls.Add(Me.picBoard)
        Me.groupNotes.Location = New System.Drawing.Point(197, 7)
        Me.groupNotes.Name = "groupNotes"
        Me.groupNotes.Size = New System.Drawing.Size(537, 535)
        Me.groupNotes.TabIndex = 4
        Me.groupNotes.TabStop = False
        '
        'groupGeneral
        '
        Me.groupGeneral.BackColor = System.Drawing.Color.Black
        Me.groupGeneral.Controls.Add(Me.boxTurn)
        Me.groupGeneral.Controls.Add(Me.boxFloor)
        Me.groupGeneral.Controls.Add(Me.lblTurn)
        Me.groupGeneral.Controls.Add(Me.lblFloor)
        Me.groupGeneral.Font = New System.Drawing.Font("Consolas", 12.0!)
        Me.groupGeneral.ForeColor = System.Drawing.Color.White
        Me.groupGeneral.Location = New System.Drawing.Point(6, 7)
        Me.groupGeneral.Name = "groupGeneral"
        Me.groupGeneral.Size = New System.Drawing.Size(185, 535)
        Me.groupGeneral.TabIndex = 3
        Me.groupGeneral.TabStop = False
        Me.groupGeneral.Text = "GENERAL"
        '
        'boxTurn
        '
        Me.boxTurn.BackColor = System.Drawing.Color.Black
        Me.boxTurn.ForeColor = System.Drawing.Color.White
        Me.boxTurn.Location = New System.Drawing.Point(66, 49)
        Me.boxTurn.Maximum = New Decimal(New Integer() {999999, 0, 0, 0})
        Me.boxTurn.Name = "boxTurn"
        Me.boxTurn.Size = New System.Drawing.Size(113, 26)
        Me.boxTurn.TabIndex = 196
        Me.boxTurn.TextAlign = System.Windows.Forms.HorizontalAlignment.Right
        '
        'boxFloor
        '
        Me.boxFloor.BackColor = System.Drawing.Color.Black
        Me.boxFloor.Enabled = False
        Me.boxFloor.ForeColor = System.Drawing.Color.White
        Me.boxFloor.Location = New System.Drawing.Point(66, 20)
        Me.boxFloor.Maximum = New Decimal(New Integer() {999999, 0, 0, 0})
        Me.boxFloor.Name = "boxFloor"
        Me.boxFloor.Size = New System.Drawing.Size(113, 26)
        Me.boxFloor.TabIndex = 195
        Me.boxFloor.TextAlign = System.Windows.Forms.HorizontalAlignment.Right
        '
        'lblTurn
        '
        Me.lblTurn.AutoSize = True
        Me.lblTurn.Dock = System.Windows.Forms.DockStyle.Top
        Me.lblTurn.Font = New System.Drawing.Font("Consolas", 12.0!)
        Me.lblTurn.ForeColor = System.Drawing.Color.White
        Me.lblTurn.Location = New System.Drawing.Point(3, 51)
        Me.lblTurn.Name = "lblTurn"
        Me.lblTurn.Padding = New System.Windows.Forms.Padding(0, 0, 0, 10)
        Me.lblTurn.Size = New System.Drawing.Size(63, 29)
        Me.lblTurn.TabIndex = 1
        Me.lblTurn.Text = "TURN: "
        '
        'lblFloor
        '
        Me.lblFloor.AutoSize = True
        Me.lblFloor.Dock = System.Windows.Forms.DockStyle.Top
        Me.lblFloor.Font = New System.Drawing.Font("Consolas", 12.0!)
        Me.lblFloor.ForeColor = System.Drawing.Color.White
        Me.lblFloor.Location = New System.Drawing.Point(3, 22)
        Me.lblFloor.Name = "lblFloor"
        Me.lblFloor.Padding = New System.Windows.Forms.Padding(0, 0, 0, 10)
        Me.lblFloor.Size = New System.Drawing.Size(72, 29)
        Me.lblFloor.TabIndex = 2
        Me.lblFloor.Text = "FLOOR: "
        '
        'tabPlayer
        '
        Me.tabPlayer.BackColor = System.Drawing.Color.Black
        Me.tabPlayer.Controls.Add(Me.lblHC)
        Me.tabPlayer.Controls.Add(Me.pnlHC)
        Me.tabPlayer.Controls.Add(Me.lblSC)
        Me.tabPlayer.Controls.Add(Me.pnlSC)
        Me.tabPlayer.Controls.Add(Me.tabPortrait)
        Me.tabPlayer.Controls.Add(Me.picPreview)
        Me.tabPlayer.Controls.Add(Me.playerDivider)
        Me.tabPlayer.Controls.Add(Me.lblGold)
        Me.tabPlayer.Controls.Add(Me.boxEvd)
        Me.tabPlayer.Controls.Add(Me.lblEvd)
        Me.tabPlayer.Controls.Add(Me.boxSpd)
        Me.tabPlayer.Controls.Add(Me.lblSpd)
        Me.tabPlayer.Controls.Add(Me.boxWil)
        Me.tabPlayer.Controls.Add(Me.lblWil)
        Me.tabPlayer.Controls.Add(Me.boxDef)
        Me.tabPlayer.Controls.Add(Me.lblDef)
        Me.tabPlayer.Controls.Add(Me.boxAtk)
        Me.tabPlayer.Controls.Add(Me.lblAtk)
        Me.tabPlayer.Controls.Add(Me.boxGold)
        Me.tabPlayer.Controls.Add(Me.boxHunger)
        Me.tabPlayer.Controls.Add(Me.lblHunger)
        Me.tabPlayer.Controls.Add(Me.boxMaxMana)
        Me.tabPlayer.Controls.Add(Me.lblOf2)
        Me.tabPlayer.Controls.Add(Me.boxMana)
        Me.tabPlayer.Controls.Add(Me.lblMana)
        Me.tabPlayer.Controls.Add(Me.boxMaxHealth)
        Me.tabPlayer.Controls.Add(Me.lblOf1)
        Me.tabPlayer.Controls.Add(Me.boxHealth)
        Me.tabPlayer.Controls.Add(Me.lblHealth)
        Me.tabPlayer.Controls.Add(Me.boxForm)
        Me.tabPlayer.Controls.Add(Me.boxName)
        Me.tabPlayer.Controls.Add(Me.boxSex)
        Me.tabPlayer.Controls.Add(Me.lblTitle)
        Me.tabPlayer.Controls.Add(Me.lblSex)
        Me.tabPlayer.Controls.Add(Me.lblName)
        Me.tabPlayer.Location = New System.Drawing.Point(4, 24)
        Me.tabPlayer.Name = "tabPlayer"
        Me.tabPlayer.Padding = New System.Windows.Forms.Padding(3)
        Me.tabPlayer.Size = New System.Drawing.Size(742, 548)
        Me.tabPlayer.TabIndex = 1
        Me.tabPlayer.Text = "PLAYER"
        '
        'lblHC
        '
        Me.lblHC.AutoSize = True
        Me.lblHC.Font = New System.Drawing.Font("Consolas", 12.0!)
        Me.lblHC.ForeColor = System.Drawing.Color.White
        Me.lblHC.Location = New System.Drawing.Point(250, 210)
        Me.lblHC.Name = "lblHC"
        Me.lblHC.Size = New System.Drawing.Size(99, 19)
        Me.lblHC.TabIndex = 266
        Me.lblHC.Text = "HAIR COLOR"
        '
        'pnlHC
        '
        Me.pnlHC.Location = New System.Drawing.Point(357, 211)
        Me.pnlHC.Name = "pnlHC"
        Me.pnlHC.Size = New System.Drawing.Size(134, 26)
        Me.pnlHC.TabIndex = 265
        '
        'lblSC
        '
        Me.lblSC.AutoSize = True
        Me.lblSC.Font = New System.Drawing.Font("Consolas", 12.0!)
        Me.lblSC.ForeColor = System.Drawing.Color.White
        Me.lblSC.Location = New System.Drawing.Point(3, 210)
        Me.lblSC.Name = "lblSC"
        Me.lblSC.Size = New System.Drawing.Size(99, 19)
        Me.lblSC.TabIndex = 264
        Me.lblSC.Text = "SKIN COLOR"
        '
        'pnlSC
        '
        Me.pnlSC.Location = New System.Drawing.Point(110, 211)
        Me.pnlSC.Name = "pnlSC"
        Me.pnlSC.Size = New System.Drawing.Size(134, 26)
        Me.pnlSC.TabIndex = 263
        '
        'tabPortrait
        '
        Me.tabPortrait.Controls.Add(Me.tabPageBackground)
        Me.tabPortrait.Controls.Add(Me.tabPageRearHair)
        Me.tabPortrait.Controls.Add(Me.tabPageBody)
        Me.tabPortrait.Controls.Add(Me.tabPageClothing)
        Me.tabPortrait.Controls.Add(Me.tabPageFace)
        Me.tabPortrait.Controls.Add(Me.tabPageMiddleHair)
        Me.tabPortrait.Controls.Add(Me.tabPageEars)
        Me.tabPortrait.Controls.Add(Me.tabPageNose)
        Me.tabPortrait.Controls.Add(Me.tabPageMouth)
        Me.tabPortrait.Controls.Add(Me.tabPageEyes)
        Me.tabPortrait.Controls.Add(Me.tabPageEyebrows)
        Me.tabPortrait.Controls.Add(Me.tabPageFaceMark)
        Me.tabPortrait.Controls.Add(Me.tabPageGlasses)
        Me.tabPortrait.Controls.Add(Me.tabPageCloak)
        Me.tabPortrait.Controls.Add(Me.tabPageAccessories)
        Me.tabPortrait.Controls.Add(Me.tabPageFrontHair)
        Me.tabPortrait.Controls.Add(Me.tabPageHat)
        Me.tabPortrait.Location = New System.Drawing.Point(3, 254)
        Me.tabPortrait.Name = "tabPortrait"
        Me.tabPortrait.SelectedIndex = 0
        Me.tabPortrait.Size = New System.Drawing.Size(736, 291)
        Me.tabPortrait.TabIndex = 262
        '
        'tabPageBackground
        '
        Me.tabPageBackground.AutoScroll = True
        Me.tabPageBackground.BackColor = System.Drawing.Color.Black
        Me.tabPageBackground.Location = New System.Drawing.Point(4, 24)
        Me.tabPageBackground.Name = "tabPageBackground"
        Me.tabPageBackground.Padding = New System.Windows.Forms.Padding(3)
        Me.tabPageBackground.Size = New System.Drawing.Size(728, 263)
        Me.tabPageBackground.TabIndex = 0
        Me.tabPageBackground.Text = "BACKGROUND"
        '
        'tabPageRearHair
        '
        Me.tabPageRearHair.AutoScroll = True
        Me.tabPageRearHair.BackColor = System.Drawing.Color.Black
        Me.tabPageRearHair.Location = New System.Drawing.Point(4, 24)
        Me.tabPageRearHair.Name = "tabPageRearHair"
        Me.tabPageRearHair.Padding = New System.Windows.Forms.Padding(3)
        Me.tabPageRearHair.Size = New System.Drawing.Size(728, 263)
        Me.tabPageRearHair.TabIndex = 1
        Me.tabPageRearHair.Text = "REAR HAIR"
        '
        'tabPageBody
        '
        Me.tabPageBody.AutoScroll = True
        Me.tabPageBody.BackColor = System.Drawing.Color.Black
        Me.tabPageBody.Location = New System.Drawing.Point(4, 24)
        Me.tabPageBody.Name = "tabPageBody"
        Me.tabPageBody.Padding = New System.Windows.Forms.Padding(3)
        Me.tabPageBody.Size = New System.Drawing.Size(728, 263)
        Me.tabPageBody.TabIndex = 2
        Me.tabPageBody.Text = "BODY"
        '
        'tabPageClothing
        '
        Me.tabPageClothing.AutoScroll = True
        Me.tabPageClothing.BackColor = System.Drawing.Color.Black
        Me.tabPageClothing.Location = New System.Drawing.Point(4, 24)
        Me.tabPageClothing.Name = "tabPageClothing"
        Me.tabPageClothing.Padding = New System.Windows.Forms.Padding(3)
        Me.tabPageClothing.Size = New System.Drawing.Size(728, 263)
        Me.tabPageClothing.TabIndex = 3
        Me.tabPageClothing.Text = "CLOTHING"
        '
        'tabPageFace
        '
        Me.tabPageFace.AutoScroll = True
        Me.tabPageFace.BackColor = System.Drawing.Color.Black
        Me.tabPageFace.Location = New System.Drawing.Point(4, 24)
        Me.tabPageFace.Name = "tabPageFace"
        Me.tabPageFace.Size = New System.Drawing.Size(728, 263)
        Me.tabPageFace.TabIndex = 4
        Me.tabPageFace.Text = "FACE"
        '
        'tabPageMiddleHair
        '
        Me.tabPageMiddleHair.AutoScroll = True
        Me.tabPageMiddleHair.BackColor = System.Drawing.Color.Black
        Me.tabPageMiddleHair.Location = New System.Drawing.Point(4, 24)
        Me.tabPageMiddleHair.Name = "tabPageMiddleHair"
        Me.tabPageMiddleHair.Size = New System.Drawing.Size(728, 263)
        Me.tabPageMiddleHair.TabIndex = 5
        Me.tabPageMiddleHair.Text = "MIDDLE HAIR"
        '
        'tabPageEars
        '
        Me.tabPageEars.AutoScroll = True
        Me.tabPageEars.BackColor = System.Drawing.Color.Black
        Me.tabPageEars.Location = New System.Drawing.Point(4, 24)
        Me.tabPageEars.Name = "tabPageEars"
        Me.tabPageEars.Size = New System.Drawing.Size(728, 263)
        Me.tabPageEars.TabIndex = 6
        Me.tabPageEars.Text = "EARS"
        '
        'tabPageNose
        '
        Me.tabPageNose.AutoScroll = True
        Me.tabPageNose.BackColor = System.Drawing.Color.Black
        Me.tabPageNose.Location = New System.Drawing.Point(4, 24)
        Me.tabPageNose.Name = "tabPageNose"
        Me.tabPageNose.Size = New System.Drawing.Size(728, 263)
        Me.tabPageNose.TabIndex = 7
        Me.tabPageNose.Text = "NOSE"
        '
        'tabPageMouth
        '
        Me.tabPageMouth.AutoScroll = True
        Me.tabPageMouth.BackColor = System.Drawing.Color.Black
        Me.tabPageMouth.Location = New System.Drawing.Point(4, 24)
        Me.tabPageMouth.Name = "tabPageMouth"
        Me.tabPageMouth.Size = New System.Drawing.Size(728, 263)
        Me.tabPageMouth.TabIndex = 8
        Me.tabPageMouth.Text = "MOUTH"
        '
        'tabPageEyes
        '
        Me.tabPageEyes.AutoScroll = True
        Me.tabPageEyes.BackColor = System.Drawing.Color.Black
        Me.tabPageEyes.Location = New System.Drawing.Point(4, 24)
        Me.tabPageEyes.Name = "tabPageEyes"
        Me.tabPageEyes.Size = New System.Drawing.Size(728, 263)
        Me.tabPageEyes.TabIndex = 9
        Me.tabPageEyes.Text = "EYES"
        '
        'tabPageEyebrows
        '
        Me.tabPageEyebrows.AutoScroll = True
        Me.tabPageEyebrows.BackColor = System.Drawing.Color.Black
        Me.tabPageEyebrows.Location = New System.Drawing.Point(4, 24)
        Me.tabPageEyebrows.Name = "tabPageEyebrows"
        Me.tabPageEyebrows.Size = New System.Drawing.Size(728, 263)
        Me.tabPageEyebrows.TabIndex = 10
        Me.tabPageEyebrows.Text = "EYEBROWS"
        '
        'tabPageFaceMark
        '
        Me.tabPageFaceMark.AutoScroll = True
        Me.tabPageFaceMark.BackColor = System.Drawing.Color.Black
        Me.tabPageFaceMark.Location = New System.Drawing.Point(4, 24)
        Me.tabPageFaceMark.Name = "tabPageFaceMark"
        Me.tabPageFaceMark.Size = New System.Drawing.Size(728, 263)
        Me.tabPageFaceMark.TabIndex = 11
        Me.tabPageFaceMark.Text = "FACE MARK"
        '
        'tabPageGlasses
        '
        Me.tabPageGlasses.AutoScroll = True
        Me.tabPageGlasses.BackColor = System.Drawing.Color.Black
        Me.tabPageGlasses.Location = New System.Drawing.Point(4, 24)
        Me.tabPageGlasses.Name = "tabPageGlasses"
        Me.tabPageGlasses.Size = New System.Drawing.Size(728, 263)
        Me.tabPageGlasses.TabIndex = 12
        Me.tabPageGlasses.Text = "GLASSES"
        '
        'tabPageCloak
        '
        Me.tabPageCloak.AutoScroll = True
        Me.tabPageCloak.BackColor = System.Drawing.Color.Black
        Me.tabPageCloak.Location = New System.Drawing.Point(4, 24)
        Me.tabPageCloak.Name = "tabPageCloak"
        Me.tabPageCloak.Size = New System.Drawing.Size(728, 263)
        Me.tabPageCloak.TabIndex = 13
        Me.tabPageCloak.Text = "CLOAK"
        '
        'tabPageAccessories
        '
        Me.tabPageAccessories.AutoScroll = True
        Me.tabPageAccessories.BackColor = System.Drawing.Color.Black
        Me.tabPageAccessories.Location = New System.Drawing.Point(4, 24)
        Me.tabPageAccessories.Name = "tabPageAccessories"
        Me.tabPageAccessories.Size = New System.Drawing.Size(728, 263)
        Me.tabPageAccessories.TabIndex = 14
        Me.tabPageAccessories.Text = "ACCESSORIES"
        '
        'tabPageFrontHair
        '
        Me.tabPageFrontHair.AutoScroll = True
        Me.tabPageFrontHair.BackColor = System.Drawing.Color.Black
        Me.tabPageFrontHair.Location = New System.Drawing.Point(4, 24)
        Me.tabPageFrontHair.Name = "tabPageFrontHair"
        Me.tabPageFrontHair.Size = New System.Drawing.Size(728, 263)
        Me.tabPageFrontHair.TabIndex = 15
        Me.tabPageFrontHair.Text = "FRONT HAIR"
        '
        'tabPageHat
        '
        Me.tabPageHat.AutoScroll = True
        Me.tabPageHat.BackColor = System.Drawing.Color.Black
        Me.tabPageHat.Location = New System.Drawing.Point(4, 24)
        Me.tabPageHat.Name = "tabPageHat"
        Me.tabPageHat.Size = New System.Drawing.Size(728, 263)
        Me.tabPageHat.TabIndex = 16
        Me.tabPageHat.Text = "HAT"
        '
        'picPreview
        '
        Me.picPreview.Location = New System.Drawing.Point(548, 4)
        Me.picPreview.Name = "picPreview"
        Me.picPreview.Size = New System.Drawing.Size(146, 216)
        Me.picPreview.TabIndex = 236
        Me.picPreview.TabStop = False
        '
        'playerDivider
        '
        Me.playerDivider.Location = New System.Drawing.Point(248, 3)
        Me.playerDivider.Multiline = True
        Me.playerDivider.Name = "playerDivider"
        Me.playerDivider.Size = New System.Drawing.Size(1, 225)
        Me.playerDivider.TabIndex = 235
        '
        'lblGold
        '
        Me.lblGold.AutoSize = True
        Me.lblGold.Font = New System.Drawing.Font("Consolas", 12.0!)
        Me.lblGold.ForeColor = System.Drawing.Color.White
        Me.lblGold.Location = New System.Drawing.Point(250, 181)
        Me.lblGold.Name = "lblGold"
        Me.lblGold.Size = New System.Drawing.Size(45, 19)
        Me.lblGold.TabIndex = 224
        Me.lblGold.Text = "GOLD"
        '
        'boxEvd
        '
        Me.boxEvd.BackColor = System.Drawing.Color.Black
        Me.boxEvd.Font = New System.Drawing.Font("Consolas", 12.0!)
        Me.boxEvd.ForeColor = System.Drawing.Color.White
        Me.boxEvd.Location = New System.Drawing.Point(110, 179)
        Me.boxEvd.Maximum = New Decimal(New Integer() {999999, 0, 0, 0})
        Me.boxEvd.Name = "boxEvd"
        Me.boxEvd.Size = New System.Drawing.Size(134, 26)
        Me.boxEvd.TabIndex = 223
        Me.boxEvd.TextAlign = System.Windows.Forms.HorizontalAlignment.Right
        '
        'lblEvd
        '
        Me.lblEvd.AutoSize = True
        Me.lblEvd.Font = New System.Drawing.Font("Consolas", 12.0!)
        Me.lblEvd.ForeColor = System.Drawing.Color.White
        Me.lblEvd.Location = New System.Drawing.Point(3, 181)
        Me.lblEvd.Name = "lblEvd"
        Me.lblEvd.Size = New System.Drawing.Size(36, 19)
        Me.lblEvd.TabIndex = 222
        Me.lblEvd.Text = "EVD"
        '
        'boxSpd
        '
        Me.boxSpd.BackColor = System.Drawing.Color.Black
        Me.boxSpd.Font = New System.Drawing.Font("Consolas", 12.0!)
        Me.boxSpd.ForeColor = System.Drawing.Color.White
        Me.boxSpd.Location = New System.Drawing.Point(357, 150)
        Me.boxSpd.Maximum = New Decimal(New Integer() {999999, 0, 0, 0})
        Me.boxSpd.Name = "boxSpd"
        Me.boxSpd.Size = New System.Drawing.Size(134, 26)
        Me.boxSpd.TabIndex = 221
        Me.boxSpd.TextAlign = System.Windows.Forms.HorizontalAlignment.Right
        '
        'lblSpd
        '
        Me.lblSpd.AutoSize = True
        Me.lblSpd.Font = New System.Drawing.Font("Consolas", 12.0!)
        Me.lblSpd.ForeColor = System.Drawing.Color.White
        Me.lblSpd.Location = New System.Drawing.Point(250, 152)
        Me.lblSpd.Name = "lblSpd"
        Me.lblSpd.Size = New System.Drawing.Size(36, 19)
        Me.lblSpd.TabIndex = 220
        Me.lblSpd.Text = "SPD"
        '
        'boxWil
        '
        Me.boxWil.BackColor = System.Drawing.Color.Black
        Me.boxWil.Font = New System.Drawing.Font("Consolas", 12.0!)
        Me.boxWil.ForeColor = System.Drawing.Color.White
        Me.boxWil.Location = New System.Drawing.Point(110, 150)
        Me.boxWil.Maximum = New Decimal(New Integer() {999999, 0, 0, 0})
        Me.boxWil.Name = "boxWil"
        Me.boxWil.Size = New System.Drawing.Size(134, 26)
        Me.boxWil.TabIndex = 219
        Me.boxWil.TextAlign = System.Windows.Forms.HorizontalAlignment.Right
        '
        'lblWil
        '
        Me.lblWil.AutoSize = True
        Me.lblWil.Font = New System.Drawing.Font("Consolas", 12.0!)
        Me.lblWil.ForeColor = System.Drawing.Color.White
        Me.lblWil.Location = New System.Drawing.Point(3, 152)
        Me.lblWil.Name = "lblWil"
        Me.lblWil.Size = New System.Drawing.Size(36, 19)
        Me.lblWil.TabIndex = 218
        Me.lblWil.Text = "WIL"
        '
        'boxDef
        '
        Me.boxDef.BackColor = System.Drawing.Color.Black
        Me.boxDef.Font = New System.Drawing.Font("Consolas", 12.0!)
        Me.boxDef.ForeColor = System.Drawing.Color.White
        Me.boxDef.Location = New System.Drawing.Point(357, 121)
        Me.boxDef.Maximum = New Decimal(New Integer() {999999, 0, 0, 0})
        Me.boxDef.Name = "boxDef"
        Me.boxDef.Size = New System.Drawing.Size(134, 26)
        Me.boxDef.TabIndex = 217
        Me.boxDef.TextAlign = System.Windows.Forms.HorizontalAlignment.Right
        '
        'lblDef
        '
        Me.lblDef.AutoSize = True
        Me.lblDef.Font = New System.Drawing.Font("Consolas", 12.0!)
        Me.lblDef.ForeColor = System.Drawing.Color.White
        Me.lblDef.Location = New System.Drawing.Point(250, 123)
        Me.lblDef.Name = "lblDef"
        Me.lblDef.Size = New System.Drawing.Size(36, 19)
        Me.lblDef.TabIndex = 216
        Me.lblDef.Text = "DEF"
        '
        'boxAtk
        '
        Me.boxAtk.BackColor = System.Drawing.Color.Black
        Me.boxAtk.Font = New System.Drawing.Font("Consolas", 12.0!)
        Me.boxAtk.ForeColor = System.Drawing.Color.White
        Me.boxAtk.Location = New System.Drawing.Point(110, 121)
        Me.boxAtk.Maximum = New Decimal(New Integer() {999999, 0, 0, 0})
        Me.boxAtk.Name = "boxAtk"
        Me.boxAtk.Size = New System.Drawing.Size(134, 26)
        Me.boxAtk.TabIndex = 215
        Me.boxAtk.TextAlign = System.Windows.Forms.HorizontalAlignment.Right
        '
        'lblAtk
        '
        Me.lblAtk.AutoSize = True
        Me.lblAtk.Font = New System.Drawing.Font("Consolas", 12.0!)
        Me.lblAtk.ForeColor = System.Drawing.Color.White
        Me.lblAtk.Location = New System.Drawing.Point(3, 123)
        Me.lblAtk.Name = "lblAtk"
        Me.lblAtk.Size = New System.Drawing.Size(36, 19)
        Me.lblAtk.TabIndex = 214
        Me.lblAtk.Text = "ATK"
        '
        'boxGold
        '
        Me.boxGold.BackColor = System.Drawing.Color.Black
        Me.boxGold.Font = New System.Drawing.Font("Consolas", 12.0!)
        Me.boxGold.ForeColor = System.Drawing.Color.White
        Me.boxGold.Location = New System.Drawing.Point(357, 179)
        Me.boxGold.Maximum = New Decimal(New Integer() {999999, 0, 0, 0})
        Me.boxGold.Name = "boxGold"
        Me.boxGold.Size = New System.Drawing.Size(134, 26)
        Me.boxGold.TabIndex = 213
        Me.boxGold.TextAlign = System.Windows.Forms.HorizontalAlignment.Right
        '
        'boxHunger
        '
        Me.boxHunger.BackColor = System.Drawing.Color.Black
        Me.boxHunger.Font = New System.Drawing.Font("Consolas", 12.0!)
        Me.boxHunger.ForeColor = System.Drawing.Color.White
        Me.boxHunger.Location = New System.Drawing.Point(357, 34)
        Me.boxHunger.Maximum = New Decimal(New Integer() {999999, 0, 0, 0})
        Me.boxHunger.Name = "boxHunger"
        Me.boxHunger.Size = New System.Drawing.Size(134, 26)
        Me.boxHunger.TabIndex = 228
        Me.boxHunger.TextAlign = System.Windows.Forms.HorizontalAlignment.Right
        '
        'lblHunger
        '
        Me.lblHunger.AutoSize = True
        Me.lblHunger.Font = New System.Drawing.Font("Consolas", 12.0!)
        Me.lblHunger.ForeColor = System.Drawing.Color.White
        Me.lblHunger.Location = New System.Drawing.Point(250, 36)
        Me.lblHunger.Name = "lblHunger"
        Me.lblHunger.Size = New System.Drawing.Size(63, 19)
        Me.lblHunger.TabIndex = 229
        Me.lblHunger.Text = "HUNGER"
        '
        'boxMaxMana
        '
        Me.boxMaxMana.BackColor = System.Drawing.Color.Black
        Me.boxMaxMana.Font = New System.Drawing.Font("Consolas", 12.0!)
        Me.boxMaxMana.ForeColor = System.Drawing.Color.White
        Me.boxMaxMana.Location = New System.Drawing.Point(357, 92)
        Me.boxMaxMana.Maximum = New Decimal(New Integer() {999999, 0, 0, 0})
        Me.boxMaxMana.Name = "boxMaxMana"
        Me.boxMaxMana.Size = New System.Drawing.Size(134, 26)
        Me.boxMaxMana.TabIndex = 231
        Me.boxMaxMana.TextAlign = System.Windows.Forms.HorizontalAlignment.Right
        '
        'lblOf2
        '
        Me.lblOf2.Font = New System.Drawing.Font("Consolas", 12.0!)
        Me.lblOf2.ForeColor = System.Drawing.Color.White
        Me.lblOf2.Location = New System.Drawing.Point(244, 93)
        Me.lblOf2.Name = "lblOf2"
        Me.lblOf2.Size = New System.Drawing.Size(114, 24)
        Me.lblOf2.TabIndex = 233
        Me.lblOf2.Text = "--OUT OF--"
        Me.lblOf2.TextAlign = System.Drawing.ContentAlignment.MiddleCenter
        '
        'boxMana
        '
        Me.boxMana.BackColor = System.Drawing.Color.Black
        Me.boxMana.Font = New System.Drawing.Font("Consolas", 12.0!)
        Me.boxMana.ForeColor = System.Drawing.Color.White
        Me.boxMana.Location = New System.Drawing.Point(110, 92)
        Me.boxMana.Maximum = New Decimal(New Integer() {999999, 0, 0, 0})
        Me.boxMana.Name = "boxMana"
        Me.boxMana.Size = New System.Drawing.Size(134, 26)
        Me.boxMana.TabIndex = 230
        Me.boxMana.TextAlign = System.Windows.Forms.HorizontalAlignment.Right
        '
        'lblMana
        '
        Me.lblMana.AutoSize = True
        Me.lblMana.Font = New System.Drawing.Font("Consolas", 12.0!)
        Me.lblMana.ForeColor = System.Drawing.Color.White
        Me.lblMana.Location = New System.Drawing.Point(3, 96)
        Me.lblMana.Name = "lblMana"
        Me.lblMana.Size = New System.Drawing.Size(45, 19)
        Me.lblMana.TabIndex = 227
        Me.lblMana.Text = "MANA"
        '
        'boxMaxHealth
        '
        Me.boxMaxHealth.BackColor = System.Drawing.Color.Black
        Me.boxMaxHealth.Font = New System.Drawing.Font("Consolas", 12.0!)
        Me.boxMaxHealth.ForeColor = System.Drawing.Color.White
        Me.boxMaxHealth.Location = New System.Drawing.Point(357, 63)
        Me.boxMaxHealth.Maximum = New Decimal(New Integer() {999999, 0, 0, 0})
        Me.boxMaxHealth.Name = "boxMaxHealth"
        Me.boxMaxHealth.Size = New System.Drawing.Size(134, 26)
        Me.boxMaxHealth.TabIndex = 234
        Me.boxMaxHealth.TextAlign = System.Windows.Forms.HorizontalAlignment.Right
        '
        'lblOf1
        '
        Me.lblOf1.Font = New System.Drawing.Font("Consolas", 12.0!)
        Me.lblOf1.ForeColor = System.Drawing.Color.White
        Me.lblOf1.Location = New System.Drawing.Point(244, 65)
        Me.lblOf1.Name = "lblOf1"
        Me.lblOf1.Size = New System.Drawing.Size(114, 24)
        Me.lblOf1.TabIndex = 232
        Me.lblOf1.Text = "--OUT OF--"
        Me.lblOf1.TextAlign = System.Drawing.ContentAlignment.MiddleCenter
        '
        'boxHealth
        '
        Me.boxHealth.BackColor = System.Drawing.Color.Black
        Me.boxHealth.Font = New System.Drawing.Font("Consolas", 12.0!)
        Me.boxHealth.ForeColor = System.Drawing.Color.White
        Me.boxHealth.Location = New System.Drawing.Point(110, 63)
        Me.boxHealth.Maximum = New Decimal(New Integer() {999999, 0, 0, 0})
        Me.boxHealth.Name = "boxHealth"
        Me.boxHealth.Size = New System.Drawing.Size(134, 26)
        Me.boxHealth.TabIndex = 226
        Me.boxHealth.TextAlign = System.Windows.Forms.HorizontalAlignment.Right
        '
        'lblHealth
        '
        Me.lblHealth.AutoSize = True
        Me.lblHealth.Font = New System.Drawing.Font("Consolas", 12.0!)
        Me.lblHealth.ForeColor = System.Drawing.Color.White
        Me.lblHealth.Location = New System.Drawing.Point(3, 65)
        Me.lblHealth.Name = "lblHealth"
        Me.lblHealth.Size = New System.Drawing.Size(63, 19)
        Me.lblHealth.TabIndex = 225
        Me.lblHealth.Text = "HEALTH"
        '
        'boxForm
        '
        Me.boxForm.BackColor = System.Drawing.Color.Black
        Me.boxForm.Enabled = False
        Me.boxForm.Font = New System.Drawing.Font("Consolas", 12.0!)
        Me.boxForm.ForeColor = System.Drawing.Color.White
        Me.boxForm.FormattingEnabled = True
        Me.boxForm.Location = New System.Drawing.Point(110, 33)
        Me.boxForm.Name = "boxForm"
        Me.boxForm.Size = New System.Drawing.Size(134, 27)
        Me.boxForm.TabIndex = 212
        '
        'boxName
        '
        Me.boxName.BackColor = System.Drawing.Color.Black
        Me.boxName.Font = New System.Drawing.Font("Consolas", 12.0!)
        Me.boxName.ForeColor = System.Drawing.Color.White
        Me.boxName.Location = New System.Drawing.Point(110, 4)
        Me.boxName.Name = "boxName"
        Me.boxName.Size = New System.Drawing.Size(134, 26)
        Me.boxName.TabIndex = 211
        '
        'boxSex
        '
        Me.boxSex.BackColor = System.Drawing.Color.Black
        Me.boxSex.Font = New System.Drawing.Font("Consolas", 12.0!)
        Me.boxSex.ForeColor = System.Drawing.Color.White
        Me.boxSex.FormattingEnabled = True
        Me.boxSex.Location = New System.Drawing.Point(357, 3)
        Me.boxSex.Name = "boxSex"
        Me.boxSex.Size = New System.Drawing.Size(134, 27)
        Me.boxSex.TabIndex = 207
        '
        'lblTitle
        '
        Me.lblTitle.AutoSize = True
        Me.lblTitle.Font = New System.Drawing.Font("Consolas", 12.0!)
        Me.lblTitle.ForeColor = System.Drawing.Color.White
        Me.lblTitle.Location = New System.Drawing.Point(3, 36)
        Me.lblTitle.Name = "lblTitle"
        Me.lblTitle.Size = New System.Drawing.Size(54, 19)
        Me.lblTitle.TabIndex = 210
        Me.lblTitle.Text = "TITLE"
        '
        'lblSex
        '
        Me.lblSex.AutoSize = True
        Me.lblSex.Font = New System.Drawing.Font("Consolas", 12.0!)
        Me.lblSex.ForeColor = System.Drawing.Color.White
        Me.lblSex.Location = New System.Drawing.Point(250, 7)
        Me.lblSex.Name = "lblSex"
        Me.lblSex.Size = New System.Drawing.Size(36, 19)
        Me.lblSex.TabIndex = 208
        Me.lblSex.Text = "SEX"
        '
        'lblName
        '
        Me.lblName.AutoSize = True
        Me.lblName.Font = New System.Drawing.Font("Consolas", 12.0!)
        Me.lblName.ForeColor = System.Drawing.Color.White
        Me.lblName.Location = New System.Drawing.Point(3, 7)
        Me.lblName.Name = "lblName"
        Me.lblName.Size = New System.Drawing.Size(45, 19)
        Me.lblName.TabIndex = 209
        Me.lblName.Text = "NAME"
        '
        'tabInventory
        '
        Me.tabInventory.BackColor = System.Drawing.Color.Black
        Me.tabInventory.Controls.Add(Me.number)
        Me.tabInventory.Controls.Add(Me.btnRemove)
        Me.tabInventory.Controls.Add(Me.boxInventory)
        Me.tabInventory.Controls.Add(Me.btnAdd)
        Me.tabInventory.Controls.Add(Me.lblInventory)
        Me.tabInventory.Controls.Add(Me.boxItems)
        Me.tabInventory.Controls.Add(Me.lblItems)
        Me.tabInventory.Location = New System.Drawing.Point(4, 24)
        Me.tabInventory.Name = "tabInventory"
        Me.tabInventory.Padding = New System.Windows.Forms.Padding(3)
        Me.tabInventory.Size = New System.Drawing.Size(742, 548)
        Me.tabInventory.TabIndex = 2
        Me.tabInventory.Text = "INVENTORY"
        '
        'number
        '
        Me.number.BackColor = System.Drawing.Color.Black
        Me.number.Font = New System.Drawing.Font("Consolas", 12.0!)
        Me.number.ForeColor = System.Drawing.Color.White
        Me.number.Location = New System.Drawing.Point(327, 261)
        Me.number.Maximum = New Decimal(New Integer() {999999, 0, 0, 0})
        Me.number.Name = "number"
        Me.number.Size = New System.Drawing.Size(89, 26)
        Me.number.TabIndex = 183
        Me.number.TextAlign = System.Windows.Forms.HorizontalAlignment.Center
        '
        'btnRemove
        '
        Me.btnRemove.BackColor = System.Drawing.Color.Black
        Me.btnRemove.Font = New System.Drawing.Font("Consolas", 8.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.btnRemove.ForeColor = System.Drawing.Color.White
        Me.btnRemove.Location = New System.Drawing.Point(292, 308)
        Me.btnRemove.Name = "btnRemove"
        Me.btnRemove.Size = New System.Drawing.Size(89, 36)
        Me.btnRemove.TabIndex = 182
        Me.btnRemove.Text = "Remove -->"
        Me.btnRemove.UseVisualStyleBackColor = False
        '
        'boxInventory
        '
        Me.boxInventory.BackColor = System.Drawing.Color.Black
        Me.boxInventory.Font = New System.Drawing.Font("Consolas", 12.0!)
        Me.boxInventory.ForeColor = System.Drawing.Color.White
        Me.boxInventory.FormattingEnabled = True
        Me.boxInventory.ItemHeight = 19
        Me.boxInventory.Location = New System.Drawing.Point(6, 6)
        Me.boxInventory.Name = "boxInventory"
        Me.boxInventory.SelectionMode = System.Windows.Forms.SelectionMode.MultiExtended
        Me.boxInventory.Size = New System.Drawing.Size(280, 536)
        Me.boxInventory.Sorted = True
        Me.boxInventory.TabIndex = 8
        '
        'btnAdd
        '
        Me.btnAdd.BackColor = System.Drawing.Color.Black
        Me.btnAdd.Font = New System.Drawing.Font("Consolas", 8.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.btnAdd.ForeColor = System.Drawing.Color.White
        Me.btnAdd.Location = New System.Drawing.Point(361, 203)
        Me.btnAdd.Name = "btnAdd"
        Me.btnAdd.Size = New System.Drawing.Size(89, 36)
        Me.btnAdd.TabIndex = 181
        Me.btnAdd.Text = "<-- Add"
        Me.btnAdd.UseVisualStyleBackColor = False
        '
        'lblInventory
        '
        Me.lblInventory.AutoSize = True
        Me.lblInventory.Font = New System.Drawing.Font("Consolas", 12.0!)
        Me.lblInventory.ForeColor = System.Drawing.Color.White
        Me.lblInventory.Location = New System.Drawing.Point(288, 6)
        Me.lblInventory.Name = "lblInventory"
        Me.lblInventory.Size = New System.Drawing.Size(90, 19)
        Me.lblInventory.TabIndex = 6
        Me.lblInventory.Text = "INVENTORY"
        '
        'boxItems
        '
        Me.boxItems.BackColor = System.Drawing.Color.Black
        Me.boxItems.Font = New System.Drawing.Font("Consolas", 12.0!)
        Me.boxItems.ForeColor = System.Drawing.Color.White
        Me.boxItems.FormattingEnabled = True
        Me.boxItems.ItemHeight = 19
        Me.boxItems.Location = New System.Drawing.Point(456, 6)
        Me.boxItems.Name = "boxItems"
        Me.boxItems.SelectionMode = System.Windows.Forms.SelectionMode.MultiExtended
        Me.boxItems.Size = New System.Drawing.Size(280, 536)
        Me.boxItems.TabIndex = 9
        '
        'lblItems
        '
        Me.lblItems.AutoSize = True
        Me.lblItems.Font = New System.Drawing.Font("Consolas", 12.0!)
        Me.lblItems.ForeColor = System.Drawing.Color.White
        Me.lblItems.Location = New System.Drawing.Point(396, 523)
        Me.lblItems.Name = "lblItems"
        Me.lblItems.Size = New System.Drawing.Size(54, 19)
        Me.lblItems.TabIndex = 7
        Me.lblItems.Text = "ITEMS"
        '
        'picBoard
        '
        Me.picBoard.Location = New System.Drawing.Point(18, 17)
        Me.picBoard.Name = "picBoard"
        Me.picBoard.Size = New System.Drawing.Size(500, 500)
        Me.picBoard.TabIndex = 0
        Me.picBoard.TabStop = False
        '
        'Debug_Window
        '
        Me.AutoScaleDimensions = New System.Drawing.SizeF(6.0!, 13.0!)
        Me.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font
        Me.BackColor = System.Drawing.Color.Black
        Me.ClientSize = New System.Drawing.Size(750, 576)
        Me.Controls.Add(Me.tabMain)
        Me.Icon = CType(resources.GetObject("$this.Icon"), System.Drawing.Icon)
        Me.Name = "Debug_Window"
        Me.SizeGripStyle = System.Windows.Forms.SizeGripStyle.Hide
        Me.StartPosition = System.Windows.Forms.FormStartPosition.CenterParent
        Me.Text = "Debug Window"
        Me.tabMain.ResumeLayout(False)
        Me.tabInformation.ResumeLayout(False)
        Me.tabGeneral.ResumeLayout(False)
        Me.groupNotes.ResumeLayout(False)
        Me.groupGeneral.ResumeLayout(False)
        Me.groupGeneral.PerformLayout()
        CType(Me.boxTurn, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.boxFloor, System.ComponentModel.ISupportInitialize).EndInit()
        Me.tabPlayer.ResumeLayout(False)
        Me.tabPlayer.PerformLayout()
        Me.tabPortrait.ResumeLayout(False)
        CType(Me.picPreview, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.boxEvd, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.boxSpd, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.boxWil, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.boxDef, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.boxAtk, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.boxGold, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.boxHunger, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.boxMaxMana, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.boxMana, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.boxMaxHealth, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.boxHealth, System.ComponentModel.ISupportInitialize).EndInit()
        Me.tabInventory.ResumeLayout(False)
        Me.tabInventory.PerformLayout()
        CType(Me.number, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.picBoard, System.ComponentModel.ISupportInitialize).EndInit()
        Me.ResumeLayout(False)

    End Sub
    Friend WithEvents tabMain As TabControl
    Friend WithEvents tabGeneral As TabPage
    Friend WithEvents tabPlayer As TabPage
    Friend WithEvents tabInventory As TabPage
    Friend WithEvents number As NumericUpDown
    Friend WithEvents btnRemove As Button
    Friend WithEvents boxInventory As ListBox
    Friend WithEvents btnAdd As Button
    Friend WithEvents lblInventory As Label
    Friend WithEvents boxItems As ListBox
    Friend WithEvents lblItems As Label
    Friend WithEvents picPreview As PictureBox
    Friend WithEvents playerDivider As TextBox
    Friend WithEvents lblGold As Label
    Friend WithEvents boxEvd As NumericUpDown
    Friend WithEvents lblEvd As Label
    Friend WithEvents boxSpd As NumericUpDown
    Friend WithEvents lblSpd As Label
    Friend WithEvents boxWil As NumericUpDown
    Friend WithEvents lblWil As Label
    Friend WithEvents boxDef As NumericUpDown
    Friend WithEvents lblDef As Label
    Friend WithEvents boxAtk As NumericUpDown
    Friend WithEvents lblAtk As Label
    Friend WithEvents boxGold As NumericUpDown
    Friend WithEvents boxHunger As NumericUpDown
    Friend WithEvents lblHunger As Label
    Friend WithEvents boxMaxMana As NumericUpDown
    Friend WithEvents lblOf2 As Label
    Friend WithEvents boxMana As NumericUpDown
    Friend WithEvents lblMana As Label
    Friend WithEvents boxMaxHealth As NumericUpDown
    Friend WithEvents lblOf1 As Label
    Friend WithEvents boxHealth As NumericUpDown
    Friend WithEvents lblHealth As Label
    Friend WithEvents boxForm As ComboBox
    Friend WithEvents boxName As TextBox
    Friend WithEvents boxSex As ComboBox
    Friend WithEvents lblTitle As Label
    Friend WithEvents lblSex As Label
    Friend WithEvents lblName As Label
    Friend WithEvents tabPortrait As TabControl
    Friend WithEvents tabPageBackground As TabPage
    Friend WithEvents tabPageRearHair As TabPage
    Friend WithEvents tabPageBody As TabPage
    Friend WithEvents tabPageClothing As TabPage
    Friend WithEvents tabPageFace As TabPage
    Friend WithEvents tabPageMiddleHair As TabPage
    Friend WithEvents tabPageEars As TabPage
    Friend WithEvents tabPageNose As TabPage
    Friend WithEvents tabPageMouth As TabPage
    Friend WithEvents tabPageEyes As TabPage
    Friend WithEvents tabPageEyebrows As TabPage
    Friend WithEvents tabPageFaceMark As TabPage
    Friend WithEvents tabPageGlasses As TabPage
    Friend WithEvents tabPageCloak As TabPage
    Friend WithEvents tabPageAccessories As TabPage
    Friend WithEvents tabPageFrontHair As TabPage
    Friend WithEvents tabPageHat As TabPage
    Friend WithEvents groupGeneral As GroupBox
    Friend WithEvents boxTurn As NumericUpDown
    Friend WithEvents boxFloor As NumericUpDown
    Friend WithEvents lblTurn As Label
    Friend WithEvents lblFloor As Label
    Friend WithEvents lblHC As Label
    Friend WithEvents pnlHC As Panel
    Friend WithEvents lblSC As Label
    Friend WithEvents pnlSC As Panel
    Friend WithEvents groupNotes As GroupBox
    Friend WithEvents tabInformation As TabPage
    Friend WithEvents boxNotes As RichTextBox
    Friend WithEvents picBoard As PictureBox
End Class
